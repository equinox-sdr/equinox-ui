![Equinox](ui/assets/logos/Equinox_ICONS_91.png)

# Equinox-UI: QT5 UI for the Equinox-SDR platform 
Equinox is a C++11 based SDR platform, especially designed for low latency
applications. This project is the QT5 based UI of the platform
that provides a block design abstraction, for developing Equinox applications.

The project is included as external git submodule at the core platform.

## Requirements
* CMake ( > 3.0)
* G++ (with C++11 support)
* yaml-cpp
* QT5
* QT5-svg

## License
Licensed under the [GPLv3](LICENSE).
