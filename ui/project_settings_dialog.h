/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROJECT_SETTINGS_DIALOG_H
#define PROJECT_SETTINGS_DIALOG_H

#include <QWidget>
#include <QDialog>
#include <QListWidget>
#include <QStackedWidget>
#include "project_settings_pages.h"
#include "project_settings.h"

class project_settings_dialog : public QDialog
{
  Q_OBJECT
public:
  project_settings_dialog(project_settings::sptr settings,
                          bool new_proj = false);

  void
  save();

public slots:
  void
  change_page(QListWidgetItem *current, QListWidgetItem *previous);

private:
  project_settings::sptr      d_pr_settings;
  project_settings_general    *d_general_settings;
  project_settings_code_gen   *d_code_settings;

  QListWidget *contents_widget;
  QStackedWidget *pages_widget;

  void
  create_icons();

  void
  load();

};

#endif // PROJECT_SETTINGS_DIALOG_H
