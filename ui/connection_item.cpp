/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018, 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "connection_item.h"
#include "utils.h"

#include <cmath>

const double connection_item::anchor_line_length = 40.0;
const double connection_item::bounding_rect_margin = 2.0;
const double connection_item::port_spacing = 5.0;
const double connection_item::pen_width = 1.5;
const double connection_item::pen_hover_width = 2.5;
const double connection_item::shape_padding = 4.0;

connection_item::connection_item(flowgraph_item_port *src,
                                 flowgraph_item_port *dst,
                                 flowgraphscene *scene)
  : QGraphicsLineItem (),
    d_src(src),
    d_dst(dst),
    d_scene(scene),
    d_hovered(false)
{
  if(!d_src || !d_dst) {
    throw std::invalid_argument("Invalid port");
  }
  d_src->add_connection(this);
  d_dst->add_connection(this);

  setFlag(QGraphicsItem::ItemIsSelectable, true);
  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
  setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
  setAcceptHoverEvents(true);
}

connection_item::~connection_item()
{
}


/**
 * @return  the source port QGraphicsItem
 */
flowgraph_item_port *
connection_item::source()
{
  return  d_src;
}

/**
 * @return  the destination port QGraphicsItem
 */
flowgraph_item_port *
connection_item::destination()
{
  return  d_dst;
}

/**
 * Recalculates the path of the connection at the flowgraph and re-draws the
 * connection line avoiding other flowgraph UI elements
 */
void
connection_item::adjust()
{
  prepareGeometryChange();
}

/**
 * Delete the connection established between the source and destination port
 */
void
connection_item::disconnect()
{
  d_src->delete_connection(this);
  d_dst->delete_connection(this);
}

QPainterPath
connection_item::shape() const
{
  QPainterPath path;
  path.addPolygon(d_shape);
  return path;
}

QRectF
connection_item::boundingRect() const
{
  QPointF src = mapFromItem(d_src, d_src->anchor());
  QPointF dst = mapFromItem(d_dst, d_dst->anchor());
  /*
   * Re-adjust the bounding rect based on the helper anchor line that each
   * port has
   */
  double rect_x = dst.x() - src.x();
  if(rect_x > 0.0) {
    src += QPointF(-anchor_line_length - port_spacing * d_src->index(), 0.0);
    dst += QPointF(anchor_line_length + d_dst->index() * port_spacing, 0.0);
  }
  else if(rect_x < 0.0 && rect_x > -2.0 * anchor_line_length) {
    src += QPointF(anchor_line_length + port_spacing * d_src->index(), 0.0);
    dst += QPointF(-anchor_line_length - port_spacing * d_dst->index(), 0.0);
  }

  return QRectF(src, dst).normalized()
      .adjusted(-bounding_rect_margin, -bounding_rect_margin,
                bounding_rect_margin, bounding_rect_margin);
}

void
connection_item::paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *option,
                       QWidget *widget)
{
  if(!d_src || !d_dst) {
    return;
  }
  painter->setRenderHint(QPainter::Antialiasing);

  QPointF src = mapFromItem(d_src, d_src->anchor());
  QPointF dst = mapFromItem(d_dst, d_dst->anchor());
  QPointF prev_dst;
  QVector<QPointF> points;
  QRectF r(src, dst);
  points << src;

  /* Add a small straight line for a nicer look */
  if(d_src->is_input()) {
    src += QPointF(-anchor_line_length - port_spacing * d_src->index(), 0.0);
    prev_dst = dst + QPointF(anchor_line_length + port_spacing* d_dst->index(),
                             0.0);
  }
  else {
    src += QPointF(anchor_line_length + port_spacing * d_src->index(), 0.0);
    prev_dst = dst + QPointF(-anchor_line_length
                             - port_spacing * d_dst->index(), 0.0);
  }
  points << src;
  points << QPointF(src.x(), r.center().y());
  points << QPointF(prev_dst.x(), r.center().y());
  points << prev_dst;
  points << dst;

  /* Re-calculate the shape */
  calc_shape(points);

  /* Set the pen color and width depending on the status of the connection */
  QPen pen;
  if(d_hovered && isSelected()) {
    pen = QPen(QApplication::palette().color(QPalette::Active,
                                             QPalette::Highlight),
               pen_hover_width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
  }
  else if(d_hovered) {
    pen = QPen(QApplication::palette().color(QPalette::Active,
                                             QPalette::Text),
               pen_hover_width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
  }
  else if(isSelected()) {
    pen = QPen(QApplication::palette().color(QPalette::Active,
                                             QPalette::Highlight),
               pen_width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
  }
  else{
    pen = QPen(QApplication::palette().color(QPalette::Active,
                                             QPalette::Text),
               pen_width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin);
  }
  painter->setPen(pen);
  painter->drawPolyline(points.data(), points.size());
}

QVariant connection_item::itemChange(QGraphicsItem::GraphicsItemChange change,
                                     const QVariant &value)
{
  /* Inform the flowgraph scene if anything changed on the selection state */
  switch(change){
    case QGraphicsItem::ItemSelectedHasChanged:
      d_scene->connection_selection_changed(this, isSelected());
      break;
    default:
      break;
  }
  return QGraphicsItem::itemChange(change, value);
}

void
connection_item::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
  d_hovered = true;
  QGraphicsItem::hoverEnterEvent(event);
  update();
}

void
connection_item::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
  d_hovered = true;
  QGraphicsItem::hoverMoveEvent(event);
  update();
}

void
connection_item::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
  d_hovered = false;
  QGraphicsItem::hoverLeaveEvent(event);
  update();
}

/**
 * @return the Euclidean distance between the source and destination port anchor
 * points
 */
double
connection_item::distance()
{
  QPointF r(mapFromItem(d_src, d_src->anchor()) -
            mapFromItem(d_dst, d_dst->anchor()));
  return std::sqrt(std::pow(r.x(), 2) + std::pow(r.y(),2));
}

/**
 * Recalculates the polygon of the shape, based on the points that contctuct the
 * connection poly-line
 * @param points a vector containing the points of the polyline
 */
void
connection_item::calc_shape(const QVector<QPointF>& points)
{
  QVector<QPointF> poly;
  /*
   * Get the points of the line and create a polygon arround it with enough
   * padding, so hovering and selection events to be easier to achieve
   */
  poly << points[0] + QPointF(0.0, -shape_padding);
  for(int i = 1; i < points.size() - 1; i++) {
    poly << points[i] + QPointF(shape_padding, -shape_padding);
  }
  poly << points[points.size() - 1] + QPointF(0.0, -shape_padding);
  poly << points[points.size() - 1] + QPointF(0, shape_padding);

  for(int i = points.size() - 2; i > 0; i--) {
    poly << points[i] + QPointF(-shape_padding, shape_padding);
  }
  poly << points[0] + QPointF(0.0, shape_padding);
  d_shape = QPolygonF(poly);
}
