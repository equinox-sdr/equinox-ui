/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_TREE_MODEL_H
#define TOOLBOX_TREE_MODEL_H

#include "toolbox.h"
#include "toolbox_tree_item.h"

#include <QAbstractItemModel>
#include <QMimeData>

class toolbox_tree_model : public QAbstractItemModel
{
  Q_OBJECT

public:
  static const char *toolbox_mime_type;

  explicit toolbox_tree_model(toolbox::sptr tools, QObject *parent = nullptr);
  ~toolbox_tree_model();

  // Header:
  QVariant
  headerData(int section, Qt::Orientation orientation,
             int role = Qt::DisplayRole) const override;

  // Basic functionality:
  QModelIndex
  index(int row, int column,
        const QModelIndex &parent = QModelIndex()) const override;

  QModelIndex
  parent(const QModelIndex &index) const override;

  int
  rowCount(const QModelIndex &parent = QModelIndex()) const override;

  int
  columnCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant
  data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

  Qt::ItemFlags
  flags(const QModelIndex &index) const override;

  QStringList
  mimeTypes() const override;

  QMimeData *
  mimeData(const QModelIndexList &indexes) const override;

private:
  void
  create_tree();

  toolbox_tree_item     *d_root;
  toolbox::sptr         d_toolboxes;

};

#endif // TOOLBOX_TREE_MODEL_H
