/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "project_settings_dialog.h"
#include "utils.h"
#include "project_settings.h"
#include <QtWidgets>

project_settings_dialog::project_settings_dialog(project_settings::sptr settings,
                                                 bool new_proj)
  : d_pr_settings(settings)
{
  contents_widget = new QListWidget;
  contents_widget->setViewMode(QListView::IconMode);
  contents_widget->setIconSize(QSize(96, 84));
  contents_widget->setMovement(QListView::Static);
  contents_widget->setMaximumWidth(128);
  contents_widget->setSpacing(12);
  contents_widget->setCurrentRow(0);

  d_general_settings = new project_settings_general();
  d_code_settings = new project_settings_code_gen();

  pages_widget = new QStackedWidget;
  pages_widget->addWidget(d_general_settings);
  pages_widget->addWidget(d_code_settings);

  create_icons();

  QPushButton *generate_button = new QPushButton(tr("Generate"));
  QPushButton *apply_button = new QPushButton(tr("Apply"));
  QPushButton *close_button = new QPushButton(tr("Close"));

  connect(apply_button, &QAbstractButton::clicked,
          this, &project_settings_dialog::save);

  connect(close_button, &QAbstractButton::clicked, this, &QWidget::close);

  QHBoxLayout *hor_layout = new QHBoxLayout;
  hor_layout->addWidget(contents_widget);
  hor_layout->addWidget(pages_widget, 1);

  QHBoxLayout *buttons_layout = new QHBoxLayout;
  buttons_layout->addStretch(1);
  buttons_layout->addWidget(generate_button);
  buttons_layout->addWidget(apply_button);
  buttons_layout->addWidget(close_button);

  QVBoxLayout *main_layout = new QVBoxLayout;
  main_layout->addLayout(hor_layout);
  main_layout->addStretch(1);
  main_layout->addSpacing(12);
  main_layout->addLayout(buttons_layout);
  setLayout(main_layout);
  setWindowTitle(tr("Project Configuration"));

  /* Set icon based on the theme */
  QFileSelector selector;
  QStringList strlist;
  if(is_dark_themed()) {
    strlist << "white";
  }
  else{
    strlist << "black";
  }
  selector.setExtraSelectors(strlist);
  setWindowIcon(QIcon(selector.select(":/assets/logos/Equinox_ICONS_95.svg")));
  if(!new_proj) {
    load();
  }
}

void
project_settings_dialog::save()
{
  /* Before saving, make some sanity checks */
  if(d_general_settings->name().length() == 0) {
    QMessageBox mb;
    mb.setText("Invalid project name");
    mb.exec();
    return;
  }

  if(d_general_settings->dir().length() == 0) {
    QMessageBox mb;
    mb.setText("Invalid project directory");
    mb.exec();
    return;
  }

  d_pr_settings->set_name(d_general_settings->name());
  d_pr_settings->set_dir(d_general_settings->dir());
  d_pr_settings->set_authors(d_general_settings->authors());
  d_pr_settings->set_license(d_general_settings->license());
  d_pr_settings->set_comments(d_general_settings->comments());
  bool success = d_pr_settings->save();
  if(!success) {
    QMessageBox mb;
    mb.setText("Could not save the project. Check the directory and/or permissions");
    mb.exec();
  }
}

void
project_settings_dialog::change_page(QListWidgetItem *current,
                              QListWidgetItem *previous)
{
  if (!current) {
      current = previous;
  }

  pages_widget->setCurrentIndex(contents_widget->row(current));
}

void
project_settings_dialog::create_icons()
{
  QFileSelector selector;
  QStringList strlist;

  if(is_dark_themed()) {
    strlist << "white";
  }
  else{
    strlist << "black";
  }
  selector.setExtraSelectors(strlist);

  /* Generic Configuration */
  QListWidgetItem *config_button = new QListWidgetItem(contents_widget);
  config_button->setIcon(QIcon(selector.select(
                                 ":/assets/logos/Equinox_ICONS_95.svg")));
  config_button->setText(tr("Configuration"));
  config_button->setTextAlignment(Qt::AlignHCenter);
  config_button->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

  /* Code generation configuration */
  QListWidgetItem *code_gen_button = new QListWidgetItem(contents_widget);
  code_gen_button->setIcon(QIcon(selector.select(
                                 ":/assets/logos/ISO_C++_Logo.svg")));
  code_gen_button->setText(tr("Code Generation"));
  code_gen_button->setTextAlignment(Qt::AlignHCenter);
  code_gen_button->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

  connect(contents_widget, &QListWidget::currentItemChanged,
          this, &project_settings_dialog::change_page);
}

void
project_settings_dialog::load()
{
  bool success = d_pr_settings->load();
  if(!success) {
    return;
  }

  /* Set the existing settings to the corresponding fields */
  d_general_settings->set_dir(d_pr_settings->dir());
  d_general_settings->set_name(d_pr_settings->name());
  d_general_settings->set_authors(d_pr_settings->authors());
  d_general_settings->set_license(d_pr_settings->license());
  d_general_settings->set_comments(d_pr_settings->comments());
}
