/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWGRAPH_ITEM_DESCRIPTION_H
#define FLOWGRAPH_ITEM_DESCRIPTION_H

#include <toolbox_item.h>

#include <QGraphicsItemGroup>
#include <QGraphicsTextItem>
#include <QMap>

class flowgraph_item;

class flowgraph_item_description : public QGraphicsItemGroup
{
public:
  static const int            titlefont_size;
  static const int            paramfont_size;
  static const int            param_vspace;

  flowgraph_item_description(toolbox_item::sptr item,
                             flowgraph_item *parent);

  ~flowgraph_item_description();

  QRectF
  boundingRect() const override;

private:
  flowgraph_item              *d_flawgraph_item;
  toolbox_item::sptr          d_toolbox_item;
  double                      d_w;
  double                      d_h;
  QGraphicsTextItem           *d_title;
  QMap<QString, QGraphicsTextItem *>
                              d_params;

};

#endif // FLOWGRAPH_ITEM_DESCRIPTION_H
