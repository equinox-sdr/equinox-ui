/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flowgraph_item_description.h"
#include "flowgraph_item.h"

const int flowgraph_item_description::titlefont_size = 10;
const int flowgraph_item_description::paramfont_size = 8;
const int flowgraph_item_description::param_vspace = 2;

flowgraph_item_description::flowgraph_item_description(toolbox_item::sptr item,
                                                       flowgraph_item *parent)
  : QGraphicsItemGroup (parent),
    d_flawgraph_item(parent),
    d_toolbox_item(item),
    d_w(0.0),
    d_h(0.0)
{
  if(!item) {
    throw std::invalid_argument("Invalid toolbox item");
  }

  /* Create the title */
  QFont f;
  d_title = new QGraphicsTextItem(this);
  f.setBold(true);
  f.setPointSize(titlefont_size);
  d_title->setFont(f);
  d_title->setPlainText(item->name());
  addToGroup(d_title);
  d_h = d_title->boundingRect().height() + param_vspace;
  d_w = std::max(d_title->boundingRect().width(), flowgraph_item::min_width);

  /* Create the params and their values */
  const QMap<QString, param::sptr>& params = item->parameters();
  f.setBold(false);
  f.setPointSize(paramfont_size);
  for(QString i : params.keys()) {
    d_params[i] = new QGraphicsTextItem(this);
    d_params[i]->setFont(f);
    d_params[i]->setHtml("<b>" + params[i]->label() + ":</b> "
                         + params[i]->value());
    d_w = std::max(d_w, d_params[i]->boundingRect().width());
    d_params[i]->setPos(0, d_h);
    addToGroup(d_params[i]);
    d_h += d_params[i]->boundingRect().height() + param_vspace;
  }
  d_title->setPos(d_w/2.0 - d_title->boundingRect().width() / 2.0, 0);
}

flowgraph_item_description::~flowgraph_item_description()
{
  delete d_title;
  qDeleteAll(d_params);
}

QRectF
flowgraph_item_description::boundingRect() const
{
  return  QRectF(0, 0, d_w, d_h);
}
