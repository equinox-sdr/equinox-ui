/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROJECT_SETTINGS_PAGES_H
#define PROJECT_SETTINGS_PAGES_H

#include <QWidget>
#include <QtWidgets>

class project_settings_general : public QWidget
{
public:
  project_settings_general(QWidget *parent = nullptr);

  QString
  name();

  void
  set_name(const QString& str);

  QString
  license();

  void
  set_license(const QString& str);

  QString
  authors();

  void
  set_authors(const QString& str);

  QString
  dir();

  void
  set_dir(const QString& str);

  QString
  comments();

  void
  set_comments(const QString& str);

private:
  QGroupBox       *d_proj_settings;
  QLabel          *d_proj_name_label;
  QLineEdit       *d_proj_name;
  QLabel          *d_proj_dir_label;
  QLineEdit       *d_proj_dir;
  QPushButton     *d_browse;
  QLabel          *d_proj_authors_label;
  QTextEdit       *d_proj_authors;
  QLabel          *d_proj_license_label;
  QTextEdit       *d_proj_license;
  QLabel          *d_proj_comments_label;
  QTextEdit       *d_proj_comments;
private slots:
  void
  dir_browser();

};

class project_settings_code_gen : public QWidget
{
  public:
  project_settings_code_gen(QWidget *parent = nullptr);

private:
  QGroupBox       *d_code_settings;
  QGroupBox       *d_build_system_settings;
};

#endif // PROJECT_SETTINGS_PAGES_H
