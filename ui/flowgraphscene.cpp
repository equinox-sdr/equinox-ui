/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flowgraphscene.h"
#include "toolbox_tree_model.h"
#include "flowgraph_item.h"
#include "connection_item.h"

#include <QMimeData>
#include <QGraphicsSceneDragDropEvent>

flowgraphscene::flowgraphscene(project_settings::sptr settings,
                               toolbox::sptr toolbox,
                               QMenu *itemMenu,
                               int grid_size,
                               QObject *parent)
  : QGraphicsScene (parent),
    d_settings(settings),
    d_dirty(false),
    d_port_mousePress(false),
    d_grid_size(grid_size),
    d_tmp_src(nullptr),
    d_tmp_dst(nullptr),
    d_toolbox(toolbox)
{
  if(grid_size < 2) {
    throw std::invalid_argument("Grid size should be > 1");
  }
  if(!toolbox) {
    throw std::invalid_argument("Invalid toolbox");
  }
}

flowgraphscene::~flowgraphscene()
{
  qDeleteAll(d_flowgraph_items);
  qDeleteAll(d_connections);
}

/**
 * Called when a block port is selected
 * @param port
 */
void
flowgraphscene::port_selected(flowgraph_item_port *port)
{
  if(!port) {
    return;
  }

  if(port->is_input()) {
    /* An input has been already set and the new is also input. Abort...*/
    if(d_tmp_dst) {
      return;
    }
    d_tmp_dst = port;
  }
  else {
    if(d_tmp_src) {
      return;
    }
    /* An output has been already set and the new is also output. Abort...*/
    d_tmp_src = port;
  }

  if(d_tmp_src && d_tmp_dst) {
    connection_item *c = new connection_item(d_tmp_src, d_tmp_dst, this);
    d_connections.push_back(c);
    addItem(c);
    set_dirty();
    return;
  }
  d_port_mousePress = true;
}

/**
 * Called when a block selection event occurs
 * @param item the block item affected
 * @param selected true of it is selected, false otherwise
 */
void
flowgraphscene::item_selection_changed(flowgraph_item *item, bool selected)
{
  if(selected) {
    if(!d_selected_items.contains(item)) {
      d_selected_items.push_back(item);
    }
  }
  else {
    d_selected_items.removeOne(item);
  }
}

/**
 * Called when a connection selection event occurs
 * @param connection the connection item affected
 * @param selected true of it is selected, false otherwise
 */
void
flowgraphscene::connection_selection_changed(connection_item *connection,
                                             bool selected)
{
  if(selected) {
    if(!d_selected_connections.contains(connection)) {
      d_selected_connections.push_back(connection);
    }
  }
  else {
    d_selected_connections.removeOne(connection);
  }
}

/**
 * @brief Sets the size of the square grid, where elements can snap on
 * @param size
 */
void
flowgraphscene::set_grid_size(int size)
{
  if(d_grid_size > 1) {
    d_grid_size = size;
    invalidate(sceneRect(), QGraphicsScene::BackgroundLayer);
  }
}

/**
 * @return  the square grid size
 */
int
flowgraphscene::grid_size()
{
  return d_grid_size;
}

/**
 * @return  true if the flowgraph contains modifications, false otherwise
 */
bool
flowgraphscene::has_changes()
{
  return d_dirty;
}

/**
 * @return the flowgraph/project settings
 */
project_settings::sptr
flowgraphscene::settings()
{
  return d_settings;
}

void
flowgraphscene::drawBackground(QPainter *painter, const QRectF &rect)
{
  QVector<QPointF> points;

  double top = static_cast<int>(rect.top())
      - (static_cast<int>(rect.top()) % d_grid_size);
  double left = static_cast<int>(rect.left())
      - (static_cast<int>(rect.left()) % d_grid_size);
  for(double x = left; x < rect.right(); x+= d_grid_size) {
    for(double y = top; y < rect.bottom(); y+= d_grid_size) {
      points << QPointF(x, y);
    }
  }
  painter->drawPoints(points.data(), points.size());
}

void
flowgraphscene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
  const QMimeData *mime = event->mimeData();
  QByteArray b;
  QDataStream stream(&b, QIODevice::ReadOnly);
  b = mime->data(toolbox_tree_model::toolbox_mime_type);
  QString s;
  stream >> s;
}

void
flowgraphscene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
  const QMimeData *mime = event->mimeData();
}

void
flowgraphscene::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
  const QMimeData *mime = event->mimeData();
}

/**
 * At the drop event a new flowgraph item is created
 * @param event
 */
void
flowgraphscene::dropEvent(QGraphicsSceneDragDropEvent *event)
{
  const QMimeData *mime = event->mimeData();
  QByteArray b;
  QDataStream stream(&b, QIODevice::ReadOnly);

  /* Get the unique ID of the toolbox item */
  b = mime->data(toolbox_tree_model::toolbox_mime_type);
  QString s;
  stream >> s;
  insert_flowgraph_item(s, event->scenePos());
  set_dirty();
}

void
flowgraphscene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
  if (mouseEvent->button() != Qt::LeftButton) {
      return;
  }
  QGraphicsScene::mousePressEvent(mouseEvent);

  /* This was not a port press event, so cancel any port connection movement */
  if(!d_port_mousePress) {
    d_tmp_src = nullptr;
    d_tmp_dst = nullptr;
  }
  d_port_mousePress = false;
}

void
flowgraphscene::keyReleaseEvent(QKeyEvent *keyEvent)
{
  /* Delete any selected items or connections */
  if(keyEvent->key() == Qt::Key_Delete) {
    delete_selected();
    set_dirty();
  }
}

/**
 * Save project configuration, including flowgraph and connections
 */
bool
flowgraphscene::save()
{
  d_settings->set_flowgraph(to_yaml());
  return d_settings->save();
}

/**
 * Restores the flowgraph based on the information of the project file
 * @return true if the restoration was successfull, false in case of any
 * error
 */
bool
flowgraphscene::restore()
{
  const YAML::Node& flowgraph = d_settings->flowgraph();
  try {
    /* First restore the blocks */
    const YAML::Node& blocks = flowgraph["blocks"];
    for(YAML::const_iterator it = blocks.begin(); it != blocks.end(); ++it) {
      QString flowgraph_id = QString::fromStdString(it->first.as<std::string>());
      const YAML::Node& block = it->second;
      QString toolbox_id = QString::fromStdString(block["id"].as<std::string>());
      QPointF pos(block["pos"][0].as<double>(), block["pos"][1].as<double>());
      /* Restore the flowgraph items with their original IDs */
      restore_flowgraph_item(flowgraph_id, toolbox_id, pos);
    }

    /* And then the connections */
    const YAML::Node& connections = flowgraph["connections"];
    for(YAML::const_iterator it = connections.begin(); it != connections.end();
        ++it) {

      flowgraph_item *srcb = d_flowgraph_items[ QString::fromStdString(
            it->second["src_block"].as<std::string>())];
      flowgraph_item *dstb = d_flowgraph_items[ QString::fromStdString(
            it->second["dst_block"].as<std::string>())];
      flowgraph_item_port *srcp = srcb->output(QString::fromStdString(
                                                 it->second["src_port"]
                                               .as<std::string>()));
      flowgraph_item_port *dstp = dstb->input(QString::fromStdString(
                                                it->second["dst_port"].
                                              as<std::string>()));
      connection_item *c = new connection_item(srcp, dstp, this);
      d_connections.push_back(c);
      addItem(c);
    }

  }
  catch (std::exception& e) {
    return false;
  }
  return true;
}

/**
 * @brief Inserts a new item at the flowgraph. The method will automatically
 * assign a unique ID.
 *
 * @param toolboxid the toolbox id
 * @param pos the position that the new item will be placed
 */
void
flowgraphscene::insert_flowgraph_item(const QString& toolboxid,
                                      const QPointF& pos)
{
  size_t cnt = 0;
  QString key(toolboxid + "_" + QString::number(cnt++));
  while(d_flowgraph_items.contains(key)) {
    key = toolboxid + "_" + QString::number(cnt++);
  }
  flowgraph_item *item = new flowgraph_item(key, (*d_toolbox)[toolboxid],
                                            pos, nullptr, this);
  addItem(item);
  d_flowgraph_items.insert(key, item);
}

/**
 * @brief Restores a flowgraph item
 * @note: For proper compatibility, this method inserts the item with the ID
 * as it was retrieved by the project file. This is not a problem, as this method
 * is called internally, before the user can do any modification at the flowgraph
 *
 * @param id the unique flowgraph ID
 * @param toolboxid the toolbox ID
 * @param pos the position to be restored
 */
void
flowgraphscene::restore_flowgraph_item(const QString &id,
                                       const QString &toolboxid,
                                       const QPointF &pos)
{
  flowgraph_item *item = new flowgraph_item(id, (*d_toolbox)[toolboxid],
                                            pos, nullptr, this);
  addItem(item);
  d_flowgraph_items.insert(id, item);
}

/**
 * Deletes all the selected blocks and connections
 */
void
flowgraphscene::delete_selected()
{

  /* First remove the affected connections */
  for(flowgraph_item * i : d_selected_items) {
    /* Remove any affected connection */
    for(connection_item *c : d_connections) {
      if(c->destination()->flowgraph_item_ptr() == i
         || c->source()->flowgraph_item_ptr() == i) {
        removeItem(c);
        /* Remove the connection from the selection list, if it was selected */
        d_selected_connections.removeOne(c);
        d_connections.removeOne(c);
        c->disconnect();
        delete c;
      }
    }
  }

  /*
   * Now that all afected connections have been deleted, it is safe to proceed
   * deleting the blocks
   */
  for(flowgraph_item *i : d_selected_items) {
    removeItem(i);
    d_flowgraph_items.remove(i->flowgraph_id());
    delete i;
  }
  d_selected_items.clear();

  for(connection_item *i : d_selected_connections) {
    removeItem(i);
    d_connections.removeOne(i);
    i->disconnect();
    delete i;
  }
  d_selected_connections.clear();
}

/**
 * @brief flowgraphscene::set_dirty call it whenever a change was mmade that
 * shoould be saved
 */
void
flowgraphscene::set_dirty()
{
  d_dirty = true;
  emit changed();
}

/**
 * Converts the flowgraph into YAML form
 * @return a YAML node containing the blocks and connections of the flowgraph
 */
YAML::Node
flowgraphscene::to_yaml()
{
  YAML::Node flowgraph;
  YAML::Node blocks;
  YAML::Node connections;
  for(flowgraph_item *i : d_flowgraph_items) {
    YAML::Node block;
    std::string key = i->flowgraph_id().toStdString();
    block["id"] = i->toolbox_item_sptr()->id().toStdString();
    block["pos"].push_back(i->scenePos().rx());
    block["pos"].push_back(i->scenePos().ry());
    blocks[key] = block;
  }
  flowgraph["blocks"] = blocks;

  size_t cnt = 0;
  for(connection_item *i : d_connections) {
    YAML::Node conn;
     std::string key = "connection_" + std::to_string(cnt++);
     conn["src_block"] = i->source()->flowgraph_item_ptr()->flowgraph_id()
         .toStdString();
     conn["src_port"] = i->source()->name().toStdString();
     conn["dst_block"] = i->destination()->flowgraph_item_ptr()->flowgraph_id()
         .toStdString();
     conn["dst_port"] = i->destination()->name().toStdString();
     connections[key] = conn;
  }
  flowgraph["connections"] = connections;
  return  flowgraph;
}
