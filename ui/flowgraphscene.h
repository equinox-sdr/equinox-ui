/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWGRAPHSCENE_H
#define FLOWGRAPHSCENE_H

#include "toolbox.h"
#include "flowgraph_item_port.h"
#include "project_settings.h"
#include <QMenu>
#include <QGraphicsScene>

class flowgraph_item;

class flowgraphscene : public QGraphicsScene
{
  Q_OBJECT
public:
  flowgraphscene(project_settings::sptr settings,
                 toolbox::sptr toolbox, QMenu *itemMenu,
                 int grid_size = 20,
                 QObject *parent = nullptr);

  ~flowgraphscene();
  void
  port_selected(flowgraph_item_port *port);

  void
  item_selection_changed(flowgraph_item *item, bool selected);

  void
  connection_selection_changed(connection_item *connection, bool selected);

  void
  set_grid_size(int size);

  int
  grid_size();

  bool
  has_changes();

  bool
  save();

  bool
  restore();

  project_settings::sptr
  settings();

signals:
  void
  changed();

public slots:

protected:
  void
  drawBackground(QPainter *painter, const QRectF &rect);

  void
  dragEnterEvent(QGraphicsSceneDragDropEvent *event);

  void
  dragMoveEvent(QGraphicsSceneDragDropEvent *event);

  void
  dragLeaveEvent(QGraphicsSceneDragDropEvent *event);

  void
  dropEvent(QGraphicsSceneDragDropEvent *event);

  void
  mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);

  void
  keyReleaseEvent(QKeyEvent * keyEvent);

private:
  project_settings::sptr          d_settings;
  bool                            d_dirty;
  bool                            d_port_mousePress;
  int                             d_grid_size;
  flowgraph_item_port             *d_tmp_src;
  flowgraph_item_port             *d_tmp_dst;
  toolbox::sptr                   d_toolbox;
  QMap<QString, flowgraph_item *> d_flowgraph_items;
  QList<connection_item *>        d_connections;
  QList<flowgraph_item *>         d_selected_items;
  QList<connection_item *>        d_selected_connections;


  void
  insert_flowgraph_item(const QString& toolboxid, const QPointF& pos);

  void
  restore_flowgraph_item(const QString& id, const QString& toolboxid,
                         const QPointF& pos);

  void
  delete_selected();

  void
  set_dirty();

  YAML::Node
  to_yaml();
};

#endif // FLOWGRAPHSCENE_H
