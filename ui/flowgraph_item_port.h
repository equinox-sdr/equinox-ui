/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018, 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWGRAPH_ITEM_PORT_H
#define FLOWGRAPH_ITEM_PORT_H


#include "flowgraph_item_rect_priv.h"

#include <QGraphicsItemGroup>
#include <QGraphicsPolygonItem>
#include <QGraphicsTextItem>

class flowgraphscene;
class connection_item;
class flowgraph_item;

/**
 * @brief The flowgraph_item_port class is a QGraphicsItem visualizing
 * an IO port
 */
class flowgraph_item_port : public QGraphicsItemGroup
{
public:

  static const int font_size;

  /**
   * @brief spacing defines the spacing between same type (input/output) ports
   */
  static const double spacing;

  flowgraph_item_port(const QString &name,
                      bool input,
                      size_t index,
                      size_t total_ports,
                      flowgraphscene *scene,
                      flowgraph_item_rect_priv *parent,
                      flowgraph_item *parent_item);

  ~flowgraph_item_port();

  const QString&
  name();

  void
  setPen(const QPen &p);

  QPointF
  anchor();

  bool
  is_input();

  bool
  is_output();

  void
  add_connection(connection_item *c);

  void
  delete_connection(connection_item *c);

  QList<connection_item *>
  connections();

  size_t
  index();

  flowgraph_item *
  flowgraph_item_ptr();

protected:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
private:
  const QString                 d_port_name;
  const bool                    d_is_input;
  const size_t                  d_index;
  flowgraph_item                *d_flowgraph_item;
  flowgraphscene                *d_scene;
  QPolygonF                     d_polygon;
  QGraphicsPolygonItem          *d_polygon_item;
  QGraphicsTextItem             *d_name;
  QList<connection_item *>      d_edges;
};

#endif // FLOWGRAPH_ITEM_PORT_H
