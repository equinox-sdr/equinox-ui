/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "settings.h"
#include "utils.h"
#include "project_settings.h"
#include "project_settings_dialog.h"
#include <QtWidgets>

#include <iostream>


mainwindow::mainwindow()
{
  create_menu();
  create_actions();
  create_toolbars();
  create_toolbox_tree();

  QFileSelector selector;
  QStringList strlist;

  if(is_dark_themed()) {
    strlist << "white";
  }
  else{
    strlist << "black";
  }
  selector.setExtraSelectors(strlist);

  QHBoxLayout *layout = new QHBoxLayout();
  d_splitter = new QSplitter();

  /* Multiple Equinox projects on separate tabs */
  d_flowgraphs_tab = new QTabWidget;
  d_flowgraphs_tab->setMovable(true);
  d_flowgraphs_tab->setTabsClosable(true);
  d_active_tab_idx = -1;
  connect(d_flowgraphs_tab, &QTabWidget::currentChanged,
          this, &mainwindow::tab_selected);
  connect(d_flowgraphs_tab, &QTabWidget::tabCloseRequested,
          this, &mainwindow::tab_close);
  d_active_flowscene = nullptr;

  d_main_widget = new QWidget;
  layout->addWidget(d_splitter);
  d_splitter->addWidget(&d_toolbox_tree_view);
  d_splitter->addWidget(d_flowgraphs_tab);


  d_main_widget->setLayout(layout);
  setCentralWidget(d_main_widget);

  setWindowTitle(tr("Equinox"));
  setWindowIcon(QIcon(selector.select(":/assets/logos/Equinox_ICONS_95.svg")));

  restoreGeometry(d_settings.win_geometry());
  restoreState(d_settings.win_state());
  d_splitter->restoreState(d_settings.splitter_state());

  /* Restore last opened projects */
  restore_open_projects();
}

mainwindow::~mainwindow()
{
  d_settings.set_win_geometry(saveGeometry());
  d_settings.set_win_state(saveState());
  d_settings.set_splitter_state(d_splitter->saveState());
  delete_menu();
}

void
mainwindow::create_menu()
{
  d_filemenu = menuBar()->addMenu(tr("&File"));
  d_editmenu = menuBar()->addMenu(tr("&Edit"));
  d_projectmenu = menuBar()->addMenu(tr("&Project"));
  d_viewmenu = menuBar()->addMenu(tr("&View"));
  d_helpmenu = menuBar()->addMenu(tr("&Help"));
}

void
mainwindow::create_actions()
{
  QAction *act;
  act = new QAction(tr("&Settings..."), this);
  act->setStatusTip(tr("Configure the project settings"));
  d_project_actions.push_back(act);
  connect(act, &QAction::triggered, this, &mainwindow::project_settings);
  d_projectmenu->addAction(act);
  d_projectmenu->setEnabled(false);
}

void
mainwindow::delete_menu()
{

}

void
mainwindow::parse_toolbox_items()
{
  d_toolboxes = toolbox::make_shared();
}

void
mainwindow::parse_toolbox_items(const QString &conf)
{
  d_toolboxes = toolbox::make_shared(conf);
}


void
mainwindow::create_toolbox_tree()
{
  /* Create the toolbox that will hold different categories of Equinox tools */
  parse_toolbox_items();

  d_toolbox_tree_model = new toolbox_tree_model(d_toolboxes);
  d_toolbox_tree_view.setModel(d_toolbox_tree_model);
  d_toolbox_tree_view.header()->hideSection(1);
  d_toolbox_tree_view.header()->resizeSections(QHeaderView::Interactive);
  /* Drag items only towards the flowgraph canvas */
  d_toolbox_tree_view.setDragDropMode(QAbstractItemView::DragOnly);
}

void
mainwindow::delete_toolbox()
{

}



void
mainwindow::create_toolbars()
{
  /* General utilities toolbar */
  d_edit_toolbar = addToolBar(tr("Edit"));

  QFileSelector selector;
  QStringList strlist;

  if(is_dark_themed()) {
    strlist << "white";
  }
  else{
    strlist << "black";
  }
  selector.setExtraSelectors(strlist);

  d_new = new QToolButton();
  d_new->setIcon(QIcon(selector.select(":/assets/toolbars/new.svg")));
  d_new->setText(tr("New"));
  connect(d_new, &QToolButton::clicked, this,
          &mainwindow::new_project_dialog);

  d_open = new QToolButton();
  d_open->setIcon(QIcon(selector.select(
                          ":/assets/toolbars/Folder_open_font_awesome.svg")));
  d_open->setText(tr("Open"));
  connect(d_open, &QToolButton::clicked, this,
          &mainwindow::load_project_dialog);

  d_save = new QToolButton();
  d_save->setIcon(QIcon(selector.select(":/assets/toolbars/save.svg")));
  d_save->setText(tr("Save"));

  d_close = new QToolButton();
  d_close->setIcon(QIcon(selector.select(":/assets/toolbars/close_x.svg")));
  d_close->setText(tr("Close"));
  d_close->setToolTip(tr("Close"));

  d_edit_toolbar->addWidget(d_new);
  d_edit_toolbar->addWidget(d_open);
  d_edit_toolbar->addWidget(d_save);
  d_edit_toolbar->addWidget(d_close);

  /* Toolbar handling the flowgraph */
  d_flowgraph_toolbar = addToolBar(tr("Flowgraph"));

  d_generate = new QToolButton();
  d_generate->setIcon(QIcon(selector.select(":/assets/toolbars/generate.svg")));
  d_generate->setText("Generate");
  d_generate->setCheckable(true);

  d_compile = new QToolButton();
  d_compile->setIcon(QIcon(selector.select(":/assets/toolbars/compile.svg")));
  d_compile->setText("Compile");
  d_compile->setCheckable(true);

  d_exec = new QToolButton();
  d_exec->setIcon(QIcon(selector.select(":/assets/toolbars/exec.svg")));
  d_exec->setText("Execute");
  d_exec->setCheckable(true);

  d_flowgraph_toolbar->addWidget(d_generate);
  d_flowgraph_toolbar->addWidget(d_compile);
  d_flowgraph_toolbar->addWidget(d_exec);
}

void
mainwindow::delete_toolbars()
{

}

bool
mainwindow::restore_project(const QString &f)
{
  bool ret;

  project_settings::sptr s = project_settings::make_shared(f);
  if(!s->load()) {
    return false;
  }
  QSize canvas_s = d_settings.get_canvas_size();
  flowgraphscene *fs = new flowgraphscene(s, d_toolboxes, nullptr, 25, this);
  fs->setSceneRect(QRect(0, 0, canvas_s.width(), canvas_s.height()));

  QGraphicsView *gview = new QGraphicsView(fs);
  gview->setDragMode(QGraphicsView::RubberBandDrag);
  gview->setRenderHint(QPainter::Antialiasing, true);
  d_flowgraphs_tab->addTab(gview, s->name());
  connect(fs, &flowgraphscene::changed, this, &mainwindow::flowgraph_changed);

  ret = fs->restore();

  /* Enable project specific menu */
  d_projectmenu->setEnabled(true);
  return ret;
}

void
mainwindow::restore_open_projects()
{
  QStringList open = d_settings.open_projects();
  for(QString i : open) {
    restore_project(i);
  }
}

void
mainwindow::closeEvent(QCloseEvent *event)
{
  /* Store at the settings the opens flowgraphs */

  QStringList open;

  for(int i = 0; i < d_flowgraphs_tab->count(); i++) {
    QGraphicsView *gview = static_cast<QGraphicsView *>(
          d_flowgraphs_tab->widget(i));
    flowgraphscene *fl = static_cast<flowgraphscene *>(gview->scene());
    open.push_back(fl->settings()->settings_file());
  }
  d_settings.set_open_projects(open);
}

void
mainwindow::new_project_dialog()
{
  QSize canvas_s = d_settings.get_canvas_size();
  project_settings::sptr s = project_settings::make_shared();
  project_settings_dialog *ps = new project_settings_dialog(s, true);
  ps->exec();
  if(!s->valid()) {
    return;
  }

  flowgraphscene *fs = new flowgraphscene(s, d_toolboxes, nullptr, 25, this);
  fs->setSceneRect(QRect(0, 0, canvas_s.width(), canvas_s.height()));

  QGraphicsView *gview = new QGraphicsView(fs);
  gview->setDragMode(QGraphicsView::RubberBandDrag);
  gview->setRenderHint(QPainter::Antialiasing, true);
  d_flowgraphs_tab->addTab(gview, s->name());
  connect(fs, &flowgraphscene::changed, this, &mainwindow::flowgraph_changed);
  /* Enable project specific menu */
  d_projectmenu->setEnabled(true);
}

void
mainwindow::load_project_dialog()
{
  bool ret;
  QString f = QFileDialog::getOpenFileName(this, tr("Open File"), "/home",
                                           tr("Equinox project (*.eqnx)"));
  if(f.size() == 0) {
    return;
  }
  project_settings::sptr s = project_settings::make_shared(f);
  if(!s->load()) {
    return;
  }

  ret = restore_project(f);
  if(!ret) {
    QMessageBox::critical(this, "Load error",
                          "Your project could not be loaded properly. "
                          "The flowgraph may be incomplete");
  }

  /* Enable project specific menu */
  d_projectmenu->setEnabled(true);
}

void
mainwindow::tab_selected(int index)
{
  if(index < 0) {
    return;
  }
  QGraphicsView *gview = static_cast<QGraphicsView *>(d_flowgraphs_tab->widget(index));
  d_active_flowscene = static_cast<flowgraphscene *>(gview->scene());
  d_active_tab_idx = index;
}

void
mainwindow::tab_close(int index)
{
  if(index < 0) {
    return ;
  }

  if(d_active_flowscene->has_changes()) {
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,
                                  "Unsaved changes",
                                  "Your project contains unsaved changes. Save?",
                                    QMessageBox::Yes | QMessageBox::No);
    if(reply == QMessageBox::Yes) {
      d_active_flowscene->save();
    }
  }

  d_flowgraphs_tab->removeTab(index);
  if(d_flowgraphs_tab->count() == 0) {
    d_active_flowscene = nullptr;
    d_active_tab_idx = -1;
    /* Disable project specific menu */
    d_projectmenu->setEnabled(false);
  }
}

void
mainwindow::flowgraph_changed()
{
  if(d_active_tab_idx < 0) {
    return;
  }
  d_flowgraphs_tab->tabBar()->setTabTextColor(d_active_tab_idx, Qt::red);
}

void
mainwindow::project_settings()
{
  project_settings_dialog *ps =
      new project_settings_dialog(d_active_flowscene->settings(), false);
  ps->exec();
}



