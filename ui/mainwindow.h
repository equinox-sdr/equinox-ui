/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "flowgraphscene.h"
#include "settings.h"
#include "toolbox.h"
#include "toolbox_tree_model.h"

#include <QMainWindow>
#include <QGraphicsView>
#include <QtWidgets>
#include <map>
#include <memory>

namespace Ui {
  class mainwindow;
}

class mainwindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit mainwindow();
  ~mainwindow();
protected:
  void
  create_menu();

  void
  create_actions();

  void
  delete_menu();


  void
  parse_toolbox_items();

  void
  parse_toolbox_items(const QString& conf);

  void
  create_toolbox_tree();

  void
  delete_toolbox();

  void
  create_toolbars();

  void
  delete_toolbars();

  bool
  restore_project(const QString& f);

  void
  restore_open_projects();

  void
  closeEvent(QCloseEvent *event) override;

private:
  settings                      d_settings;
  toolbox::sptr                 d_toolboxes;
  QMenu*                        d_filemenu;
  QMenu*                        d_editmenu;
  QMenu*                        d_projectmenu;
  QMenu*                        d_viewmenu;
  QMenu*                        d_helpmenu;
  QList<QAction *>              d_file_actions;
  QList<QAction *>              d_edit_actions;
  QList<QAction *>              d_project_actions;
  QList<QAction *>              d_view_actions;
  QList<QAction *>              d_help_actions;
  flowgraphscene*               d_active_flowscene;
  int                           d_active_tab_idx;
  QGraphicsView*                d_flview;
  QWidget*                      d_main_widget;
  QTabWidget*                   d_flowgraphs_tab;

  std::map<std::string, QButtonGroup *>
                                d_tools_categories;
  QTreeView                     d_toolbox_tree_view;
  QSplitter                     *d_splitter;
  toolbox_tree_model            *d_toolbox_tree_model;

  QToolBar*                     d_edit_toolbar;
  QToolBar*                     d_flowgraph_toolbar;

  QToolButton*                  d_new;
  QToolButton*                  d_open;
  QToolButton*                  d_save;
  QToolButton*                  d_close;
  QToolButton*                  d_generate;
  QToolButton*                  d_compile;
  QToolButton*                  d_exec;
  QToolButton*                  d_stop;

private slots:
  void
  new_project_dialog();

  void
  load_project_dialog();

  void
  tab_selected(int index);

  void
  tab_close(int index);

  void
  flowgraph_changed();

  void
  project_settings();
};

#endif // MAINWINDOW_H
