/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWGRAPH_ITEM_RECT_PRIV_H
#define FLOWGRAPH_ITEM_RECT_PRIV_H

#include "toolbox_item.h"

#include <QGraphicsPolygonItem>

class flowgraph_item_rect_priv : public QGraphicsPolygonItem
{
public:
  flowgraph_item_rect_priv(const QSizeF &size,
                           QMenu *contextMenu,
                           QGraphicsItem *parent = nullptr);
private:
  QPolygonF           d_polygon;
};

#endif // FLOWGRAPH_ITEM_RECT_PRIV_H
