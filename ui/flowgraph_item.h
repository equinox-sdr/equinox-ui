/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWGRAPH_ITEM_H
#define FLOWGRAPH_ITEM_H

#include "toolbox_item.h"
#include "flowgraph_item_rect_priv.h"
#include "flowgraph_item_port.h"
#include "connection_item.h"
#include "flowgraphscene.h"
#include "flowgraph_item_description.h"

#include <QGraphicsItemGroup>
#include <QGraphicsTextItem>

class flowgraph_item : public QGraphicsItemGroup
{
public:
  static const double left_padding;
  static const double right_padding;
  static const double top_padding;
  static const double buttom_padding;
  static const double min_height;
  static const double min_width;

  flowgraph_item(const QString& id,
                 toolbox_item::sptr item,
                 const QPointF &topLeft,
                 QMenu *contextMenu,
                 flowgraphscene *scene);

  flowgraph_item(const QString& id,
                 toolbox_item::sptr item,
                 qreal x,
                 qreal y,
                 QMenu *contextMenu,
                 flowgraphscene *scene);


  ~flowgraph_item();

  const QString&
  flowgraph_id();

  const toolbox_item::sptr
  toolbox_item_sptr();

  QRectF
  boundingRect() const override;

  flowgraph_item_port *
  input(const QString& name);

  flowgraph_item_port *
  output(const QString& name);

protected:
  QVariant
  itemChange(GraphicsItemChange change, const QVariant &value) override;

private:
  const QString                     d_flowgraph_id;
  toolbox_item::sptr                d_toolbox_item;
  flowgraphscene                    *d_scene;
  flowgraph_item_rect_priv          *d_rect;
  QList<flowgraph_item_port *>      d_inputs;
  QList<flowgraph_item_port *>      d_outputs;
  QList<connection_item *>          d_connections;
  flowgraph_item_description        *d_description;

  void
  set_ports();
};

#endif // FLOWGRAPH_ITEM_H
