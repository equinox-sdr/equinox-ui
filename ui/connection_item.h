/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018, 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECTION_ITEM_H
#define CONNECTION_ITEM_H

#include "flowgraph_item_port.h"
#include "flowgraphscene.h"
#include <QPainter>
#include <QGraphicsLineItem>

class connection_item : public QGraphicsLineItem
{
public:
  static const double anchor_line_length;
  static const double bounding_rect_margin;
  static const double port_spacing;
  static const double pen_width;
  static const double pen_hover_width;
  static const double shape_padding;

  connection_item(flowgraph_item_port *src,
                  flowgraph_item_port *dst,
                  flowgraphscene *scene);
  ~connection_item();

  flowgraph_item_port *
  source();

  flowgraph_item_port *
  destination();

  void
  adjust();

  void
  disconnect();

  QPainterPath
  shape() const override;

  QRectF
  boundingRect() const override;

protected:
  void
  paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
        QWidget *widget) override;

  QVariant
  itemChange(GraphicsItemChange change, const QVariant &value) override;

  void
  hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
  void
  hoverMoveEvent(QGraphicsSceneHoverEvent *event) override;
  void
  hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

private:
  flowgraph_item_port           *d_src;
  flowgraph_item_port           *d_dst;
  flowgraphscene                *d_scene;
  bool                          d_hovered;
  QPolygonF                     d_shape;

  double
  distance();

  void
  calc_shape(const QVector<QPointF>& points);

};

#endif // CONNECTION_ITEM_H
