/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018, 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flowgraph_item_port.h"
#include "flowgraph_item.h"
#include "utils.h"
#include "flowgraphscene.h"
#include "connection_item.h"

const int flowgraph_item_port::font_size = 8;
const double flowgraph_item_port::spacing = 25;

flowgraph_item_port::flowgraph_item_port(const QString &name,
                                         bool input,
                                         size_t index,
                                         size_t total_ports,
                                         flowgraphscene *scene,
                                         flowgraph_item_rect_priv *parent,
                                         flowgraph_item *parent_item)
    : QGraphicsItemGroup (parent),
      d_port_name(name),
      d_is_input(input),
      d_index(index),
      d_flowgraph_item(parent_item),
      d_scene(scene)
{
  if(!parent) {
    throw std::invalid_argument("Port parent cannot be null");
  }
  if(total_ports == 0) {
    throw std::invalid_argument("Invalid total ports number");
  }
  if(!scene) {
    throw std::invalid_argument("Port should belong to a valid scene");
  }

  QRectF dim = parent->boundingRect();
  QFont f;
  f.setPointSize(font_size);
  QPainterPath path;
  QPen pen(QApplication::palette().color(QPalette::Active,
                                         QPalette::Text));

  d_name = new QGraphicsTextItem(this);
  d_name->setFont(f);
  d_name->setPlainText(name);


  /* The port rectangle  */
  path.addRect(QRectF(QPointF(0, 0),
                      QSizeF(d_name->boundingRect().width() - 4,
                      d_name->boundingRect().height() - 4)));

  d_polygon_item = new QGraphicsPolygonItem(this);
  d_polygon_item->setPen(pen);
  d_polygon = path.toFillPolygon();
  d_polygon_item->setPolygon(d_polygon);

  addToGroup(d_polygon_item);
  addToGroup(d_name);

  /*
   * Set the position of the port, based if it is input or output,
   * the index of the port and the total number of the ports
   */
  QPointF pos;

  /* Set x. Inputs are in the left most vertice of the rect */
  double x = input ? -d_polygon.boundingRect().width() : dim.width();

  double center = dim.center().y();
  double y = 0;
  if(total_ports % 2 == 0) {
    y = center - spacing/2.0 - ((total_ports - 1)/2 * spacing) + index * spacing
        - d_polygon.boundingRect().height() /2.0;
  }
  else{
    y = center - (total_ports/2 * spacing) + index * spacing
        - d_polygon.boundingRect().height() /2.0;
  }
  pos = QPointF(x, y);

  setPos(pos);

  setFlag(QGraphicsItem::ItemIsSelectable, true);
}

flowgraph_item_port::~flowgraph_item_port()
{
  delete d_polygon_item;
  delete d_name;
}

const QString &
flowgraph_item_port::name()
{
  return d_port_name;
}

void
flowgraph_item_port::setPen(const QPen &p)
{
  d_polygon_item->setPen(p);
}

/**
 * @return the anchor point of the port. This is the point from where
 * connections will start or end.
 */
QPointF
flowgraph_item_port::anchor()
{
  if(d_is_input) {
    return boundingRect().topLeft() + QPointF(0.0, boundingRect().height()/2);
  }
  else{
    return boundingRect().topRight() + QPointF(-2.0, boundingRect().height()/2);
  }
}

bool
flowgraph_item_port::is_input()
{
  return  d_is_input;
}

bool
flowgraph_item_port::is_output()
{
  return !d_is_input;
}

void
flowgraph_item_port::add_connection(connection_item *c)
{
  if(!c) {
    return;
  }
  d_edges << c;
}

void
flowgraph_item_port::delete_connection(connection_item *c)
{
  d_edges.removeOne(c);
}

QList<connection_item *>
flowgraph_item_port::connections()
{
  return d_edges;
}

/**
 * @return  the index of the port
 */
size_t
flowgraph_item_port::index()
{
  return d_index;
}

/**
 * @return the flowgraph item pointer that this port is attached
 */
flowgraph_item *
flowgraph_item_port::flowgraph_item_ptr()
{
  return d_flowgraph_item;
}

void
flowgraph_item_port::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  /* TODO */
  d_scene->port_selected(this);
}
