/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flowgraph_item.h"
#include "utils.h"

#include <QPainter>

const double  flowgraph_item::left_padding = 4.0;
const double  flowgraph_item::right_padding = 4.0;
const double  flowgraph_item::top_padding = 4.0;
const double  flowgraph_item::buttom_padding = 4.0;
const double  flowgraph_item::min_height = 60;
const double  flowgraph_item::min_width = 60;

flowgraph_item::flowgraph_item(const QString& flowgraph_id,
                               toolbox_item::sptr item,
                               const QPointF &topLeft,
                               QMenu *contextMenu,
                               flowgraphscene *scene)
  : QGraphicsItemGroup (),
    d_flowgraph_id(flowgraph_id),
    d_toolbox_item(item),
    d_scene(scene),
    d_rect(nullptr)
{
  if(!scene) {
    throw std::invalid_argument("Invalid parent QGraphicsScene");
  }
  if(!item) {
    throw std::invalid_argument("Invalid toolbox item");
  }

  double h = 0.0;
  double w = 0.0;

  /* Snap to grid! */
  int grid_size = scene->grid_size();
  double x = std::round(topLeft.x()/grid_size) * grid_size;
  double y = std::round(topLeft.y()/grid_size) * grid_size;
  setPos(QPointF(x, y));

  /* Constract the description of the flowgraph item  (title, params) */
  d_description = new flowgraph_item_description(d_toolbox_item, this);

  h = std::max(d_description->boundingRect().height(), min_height);
  w = std::max(d_description->boundingRect().width(), min_width);
  w += left_padding + right_padding;
  h += top_padding + buttom_padding;

  this->isObscured(this->boundingRect());

  /* Create the rounded box that will hold the UI elements in a single item */
  d_rect = new flowgraph_item_rect_priv(QSizeF(w, h),
                                        contextMenu, this);
  d_rect->setBrush(QBrush(QApplication::palette().color(QPalette::Active,
                                                        QPalette::Button)));
  d_rect->setZValue(d_description->zValue() - 1);

  d_description->setPos(left_padding, top_padding);
  addToGroup(d_rect);
  addToGroup(d_description);
  set_ports();


  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemIsSelectable, true);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
  setHandlesChildEvents(false);
}

flowgraph_item::flowgraph_item(const QString& flowgraph_id,
                               toolbox_item::sptr item,
                               qreal x,
                               qreal y,
                               QMenu *contextMenu,
                               flowgraphscene *scene)
  : flowgraph_item(flowgraph_id, item, QPointF(x, y), contextMenu, scene)
{
}

flowgraph_item::~flowgraph_item()
{
  qDeleteAll(d_inputs);
  qDeleteAll(d_outputs);
  delete d_description;
  delete d_rect;
}

const QString &
flowgraph_item::flowgraph_id()
{
  return d_flowgraph_id;
}

const toolbox_item::sptr
flowgraph_item::toolbox_item_sptr()
{
  return d_toolbox_item;
}

QRectF
flowgraph_item::boundingRect() const
{
  if(!d_rect) {
    return QRectF();
  }

  QPointF topleft = d_rect->boundingRect().topLeft();
  QPointF buttomright = d_rect->boundingRect().bottomRight();
  if(d_inputs.size()) {
    topleft -= QPointF(d_inputs[0]->boundingRect().width(), 0.0);
  }
  if(d_outputs.size()) {
    buttomright += QPointF(d_outputs[0]->boundingRect().width(), 0.0);
  }
  return QRectF(topleft, buttomright);
}

flowgraph_item_port *
flowgraph_item::input(const QString &name)
{
  for(flowgraph_item_port *i : d_inputs) {
    if(i->name() == name) {
      return i;
    }
  }
  return nullptr;
}

flowgraph_item_port *
flowgraph_item::output(const QString &name)
{
  for(flowgraph_item_port *i : d_outputs) {
    if(i->name() == name) {
      return i;
    }
  }
  return nullptr;
}

QVariant
flowgraph_item::itemChange(GraphicsItemChange change, const QVariant &value)
{
  QPen p;
  switch(change){
    case QGraphicsItem::ItemSelectedHasChanged:
      /* Inform the flowgraph scene */
      d_scene->item_selection_changed(this, isSelected());

      /* Change the color of the rect during selection */
      if(isSelected()) {
        p = QPen(QApplication::palette().color(QPalette::Highlight));
      }
      else{
        p = QPen(QApplication::palette().color(QPalette::Active,
                                               QPalette::Text));
      }
      d_rect->setPen(p);
      for(auto i : d_inputs) {
        i->setPen(p);
      }
      for(auto i : d_outputs) {
        i->setPen(p);
      }
      break;
    case QGraphicsItem::ItemPositionChange:
          /* Snap to grid! */
      {
        int grid_size = d_scene->grid_size();
        QPointF pos = value.toPointF();
        if (pos.x() < 0.0) {
          pos.setX(grid_size);
        }
        else if(pos.x() + boundingRect().right() > scene()->width()) {
          pos.setX(scene()->width() - boundingRect().width());
        }

        if (pos.y() < 0.0) {
          pos.setY(0.0);
        }
        else if(pos.y() + boundingRect().bottom()  > scene()->width()) {
          pos.setY(scene()->height() - boundingRect().height());
        }

        double x = std::round(pos.x()/grid_size) * grid_size;
        double y = std::round(pos.y()/grid_size) * grid_size;
        return  QPointF(x, y);
      }
    case QGraphicsItem::ItemPositionHasChanged:
      /* Re draw the affected connections */
      for(auto i : d_inputs) {
        for(auto j : i->connections()) {
          j->adjust();
        }
      }
      for(auto i : d_outputs) {
        for(auto j : i->connections()) {
          j->adjust();
        }
      }
      break;
    default:
      break;
  }
  return QGraphicsItem::itemChange(change, value);
}

void
flowgraph_item::set_ports()
{
  /* Set input and ouput ports. Note that if the flowgraph item is not
   * a block, the toolbox_item class should return zero input/output ports
   */
  const QMap<QString, port::sptr> inputs = d_toolbox_item->input_ports();
  const size_t ninputs = static_cast<const size_t>(inputs.size());

  const QMap<QString, port::sptr> outputs = d_toolbox_item->output_ports();
  const size_t noutputs = static_cast<const size_t>(outputs.size());

  size_t idx = 0;
  for(QString i : inputs.keys()) {
    flowgraph_item_port *port = new flowgraph_item_port(i, true, idx++,
                                                        ninputs, d_scene,
                                                        d_rect, this);
    d_inputs << port;
  }


  idx = 0;
  for(QString i : outputs.keys()) {
    flowgraph_item_port *port = new flowgraph_item_port(i, false, idx++,
                                                        noutputs, d_scene,
                                                        d_rect, this);
    d_outputs << port;
  }

  for(auto i : d_inputs) {
    addToGroup(i);
  }
  for(auto i : d_outputs) {
    addToGroup(i);
  }
}
