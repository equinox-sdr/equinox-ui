/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "project_settings_pages.h"
#include <QtWidgets>

project_settings_general::project_settings_general(QWidget *parent) :
  QWidget (parent)
{
  d_proj_settings = new QGroupBox(tr("Project Settings"));
  d_proj_name_label = new QLabel(tr("Project Name"));
  d_proj_name = new QLineEdit();

  d_proj_dir_label = new QLabel(tr("Project Path"));
  d_proj_dir = new QLineEdit();
  d_browse = new QPushButton(tr("Browse..."));
  connect(d_browse, &QPushButton::clicked,
          this, &project_settings_general::dir_browser);

  d_proj_license_label = new QLabel(tr("Licence"));
  d_proj_license = new QTextEdit();

  d_proj_authors_label = new QLabel(tr("Authors"));
  d_proj_authors = new QTextEdit();

  d_proj_comments_label = new QLabel(tr("Comments"));
  d_proj_comments = new QTextEdit();

  QGridLayout *proj_settings_layout = new QGridLayout;
  proj_settings_layout->addWidget(d_proj_name_label, 0, 0);
  proj_settings_layout->addWidget(d_proj_name, 0, 1);
  proj_settings_layout->addWidget(d_proj_dir_label, 1, 0);
  proj_settings_layout->addWidget(d_proj_dir, 1, 1);
  proj_settings_layout->addWidget(d_browse, 1, 2);
  proj_settings_layout->addWidget(d_proj_license_label, 2, 0);
  proj_settings_layout->addWidget(d_proj_license, 2, 1);
  proj_settings_layout->addWidget(d_proj_authors_label, 3, 0);
  proj_settings_layout->addWidget(d_proj_authors, 3, 1);
  proj_settings_layout->addWidget(d_proj_comments_label, 4, 0);
  proj_settings_layout->addWidget(d_proj_comments, 4, 1);

  d_proj_settings->setLayout(proj_settings_layout);

  QVBoxLayout *main_layout = new QVBoxLayout;
  main_layout->addWidget(d_proj_settings);
  main_layout->addSpacing(12);
  main_layout->addStretch(1);
  setLayout(main_layout);
}

QString
project_settings_general::name()
{
  return d_proj_name->text();
}

void
project_settings_general::set_name(const QString &str)
{
  d_proj_name->setText(str);
}

QString
project_settings_general::license()
{
  return d_proj_license->toPlainText();
}

void
project_settings_general::set_license(const QString &str)
{
  d_proj_license->setText(str);
}

QString
project_settings_general::authors()
{
  return d_proj_authors->toPlainText();
}

void
project_settings_general::set_authors(const QString &str)
{
  d_proj_authors->setText(str);
}

QString
project_settings_general::dir()
{
  return d_proj_dir->text();
}

void
project_settings_general::set_dir(const QString &str)
{
  d_proj_dir->setText(str);
}

QString
project_settings_general::comments()
{
  return d_proj_comments->toPlainText();
}

void
project_settings_general::set_comments(const QString &str)
{
  d_proj_comments->setText(str);
}

void
project_settings_general::dir_browser()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                              "/home",
                                              QFileDialog::ShowDirsOnly
                                              | QFileDialog::DontResolveSymlinks);
  d_proj_dir->setText(dir);
}

project_settings_code_gen::project_settings_code_gen(QWidget *parent) :
  QWidget (parent)
{
  d_code_settings = new QGroupBox(tr("Code Settings"));

  d_build_system_settings = new QGroupBox(tr("Build System"));

  QVBoxLayout *main_layout = new QVBoxLayout;
  main_layout->addWidget(d_code_settings);
  main_layout->addWidget(d_build_system_settings);
  main_layout->addSpacing(12);
  main_layout->addStretch(1);
  setLayout(main_layout);
}
