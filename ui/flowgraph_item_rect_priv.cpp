/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "flowgraph_item_rect_priv.h"
#include "utils.h"

flowgraph_item_rect_priv::flowgraph_item_rect_priv(const QSizeF &size,
                                                   QMenu *contextMenu,
                                                   QGraphicsItem *parent)
  : QGraphicsPolygonItem(parent)
{
  QPainterPath path;
  QPen pen(QApplication::palette().text().color());

  /* Construct a nice rounded polygon */
  path.addRoundedRect(QRectF(QPoint(0, 0), size), 10, 10);
  setPen(pen);
  d_polygon = path.toFillPolygon();

  setPolygon(d_polygon);
}
