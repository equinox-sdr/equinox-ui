/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toolbox_tree_model.h"

const char *
toolbox_tree_model::toolbox_mime_type = "application/x-eqnx-toolbox-item";

toolbox_tree_model::toolbox_tree_model(toolbox::sptr tools, QObject *parent)
  : QAbstractItemModel(parent),
    d_toolboxes(tools)
{
  d_root = new toolbox_tree_item("Availale Toolboxes");
  create_tree();
}

toolbox_tree_model::~toolbox_tree_model()
{
  delete d_root;
}

QVariant
toolbox_tree_model::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
  if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
      return d_root->data(section);
  }
  return QVariant();
}

QModelIndex
toolbox_tree_model::index(int row, int column, const QModelIndex &parent) const
{
  if(!hasIndex(row, column, parent)) {
    return QModelIndex();
  }

  toolbox_tree_item *parent_item;
  if(!parent.isValid()) {
    parent_item = d_root;
  }
  else{
    parent_item = static_cast<toolbox_tree_item *>(parent.internalPointer());
  }

  toolbox_tree_item *children_item = parent_item->child(row);
  if(children_item) {
    return createIndex(row, column, children_item);
  }
  else{
    return QModelIndex();
  }
}

QModelIndex
toolbox_tree_model::parent(const QModelIndex &index) const
{
  if(!index.isValid()) {
    return QModelIndex();
  }

  toolbox_tree_item *children_item
      = static_cast<toolbox_tree_item *>(index.internalPointer());
  toolbox_tree_item *parent_item = children_item->parent();
  if(parent_item == d_root) {
    return QModelIndex();
  }
  return createIndex(parent_item->row(), 0, parent_item);
}

int
toolbox_tree_model::rowCount(const QModelIndex &parent) const
{
  toolbox_tree_item *parent_item;

  if(parent.column() > 0) {
    return 0;
  }

  if (!parent.isValid()) {
    parent_item = d_root;
  }
  else {
    parent_item = static_cast<toolbox_tree_item *>(parent.internalPointer());
  }
  return parent_item->children();
}

int
toolbox_tree_model::columnCount(const QModelIndex &parent) const
{
  return 1;
}


QVariant
toolbox_tree_model::data(const QModelIndex &index, int role) const
{
  if (!index.isValid()) {
    return QVariant();
  }

  if(role != Qt::DisplayRole) {
    return QVariant();
  }

  toolbox_tree_item *item
      = static_cast<toolbox_tree_item *>(index.internalPointer());
  return item->data(index.column());
}

Qt::ItemFlags
toolbox_tree_model::flags(const QModelIndex &index) const
{
  if(!index.isValid()) {
    return Qt::NoItemFlags;
  }
  /* Only leaf nodes are allowed to be dragable */
  toolbox_tree_item *item
      = static_cast<toolbox_tree_item *>(index.internalPointer());
  if(item->children() == 0) {
    return Qt::ItemIsDragEnabled | QAbstractItemModel::flags(index);
  }
  return QAbstractItemModel::flags(index);
}

QStringList
toolbox_tree_model::mimeTypes() const
{
  return QStringList() << toolbox_mime_type;
}

QMimeData *
toolbox_tree_model::mimeData(const QModelIndexList &indexes) const
{
  QMimeData *mime = new QMimeData;
  QByteArray data;
  QDataStream stream(&data, QIODevice::WriteOnly);

  for(QModelIndex i : indexes) {
    toolbox_tree_item *item
        = static_cast<toolbox_tree_item *>(i.internalPointer());
    stream << item->id();
  }
  mime->setData(toolbox_mime_type, data);
  return mime;
}

static void
create_sub_tree(toolbox_category::sptr cat, toolbox_tree_item *parent)
{

  toolbox_tree_item *tree_item
      = new toolbox_tree_item(cat->name(),"", parent);

  /* Add elements that belong to this category */
  for(toolbox_item::sptr i : cat->items()) {
    tree_item->append_child(new toolbox_tree_item(i->name(),
                                                  i->id(),
                                                  tree_item));
  }

  /* Add recursively all the subcategories */
  for(toolbox_category::sptr i : cat->subcategories()) {
    create_sub_tree(i, tree_item);
  }
  parent->append_child(tree_item);
}


void
toolbox_tree_model::create_tree()
{
  for(toolbox_category::sptr i : d_toolboxes->top_level_categories()) {
    create_sub_tree(i, d_root);
  }
}
