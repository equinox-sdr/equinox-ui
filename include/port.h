/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PORT_H
#define PORT_H

#include <cstdint>
#include <cstdlib>
#include <QString>
#include <memory>

class port
{
public:
  typedef enum {
    PORT_IN,
    PORT_OUT
  } port_type_t;

  typedef std::shared_ptr<port> sptr;

  static sptr
  make_shared(const QString& name, const QString& data_type,
              size_t size, size_t msg_num, port_type_t type);


protected:
  port(const QString& name, const QString& data_type,
       size_t size, size_t msg_num, port_type_t type);

private:
  const QString         d_name;
  const QString         d_data_type;
  const size_t          d_size;
  const size_t          d_msg_num;
  const port_type_t     d_type;
};

#endif // PORT_H
