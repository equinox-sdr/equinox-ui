/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_TREE_ITEM_H
#define TOOLBOX_TREE_ITEM_H

#include <QList>
#include <QVariant>

class toolbox_tree_item
{
public:
  explicit toolbox_tree_item(const QVariant &data,
                             const QString& id = "",
                             toolbox_tree_item *parent_item = nullptr);

  ~toolbox_tree_item();

  void
  append_child(toolbox_tree_item *child);

  toolbox_tree_item *
  child(int row);

  int
  children() const;

  int
  columns() const;

  QVariant
  data(int column) const;

  int
  row() const;

  toolbox_tree_item *
  parent();

  QString
  id() const;

private:
  QVariant                          d_item_data;
  const QString                     d_id;
  toolbox_tree_item                 *d_parent_item;
  QList<toolbox_tree_item*>         d_child_items;
};

#endif // TOOLBOX_TREE_ITEM_H
