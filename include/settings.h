/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include <QSettings>
#include <QString>
#include <QSize>


class settings
{
public:
  explicit settings();
  explicit settings(const QString& conf);

  QList<QString>
  get_kernel_desc_paths();

  QSize
  get_canvas_size();

  QByteArray
  win_geometry();

  QByteArray
  win_state();

  QByteArray
  splitter_state();

  void
  set_win_geometry(QByteArray b);

  void
  set_win_state(QByteArray b);

  void
  set_splitter_state(QByteArray b);

  void
  set_open_projects(const QStringList& projects);

  QStringList
  open_projects();

private:
  QSettings                       d_settings;

  void
  check();
};

#endif // SETTINGS_H
