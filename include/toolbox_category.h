/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_CATEGORY_H
#define TOOLBOX_CATEGORY_H

#include "toolbox_item.h"

#include <memory>
#include <map>
#include <deque>

class toolbox_category
{
public:
  typedef std::shared_ptr<toolbox_category> sptr;

  static sptr
  make_shared(const QString &name);

  sptr
  add_subcategory(const QString &name);

  void
  add_item(toolbox_item::sptr item);

  QString
  name();

  size_t
  total_items_num();

  size_t
  items_num();

  std::deque<sptr>
  subcategories();

  std::deque<toolbox_item::sptr>
  items();

  friend
  std::ostream& operator<<(std::ostream &out, const toolbox_category &c);

private:
  toolbox_category(const QString& name);

  size_t
  subcategories_num(size_t depth = 1);

  QString                                     d_name;
  std::deque<toolbox_item::sptr>              d_items;
  std::map<QString, toolbox_category::sptr>   d_subcategories;
};

#endif // TOOLBOX_CATEGORY_H
