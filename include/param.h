/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAM_H
#define PARAM_H

#include <QString>
#include <memory>
/**
 * @brief The param class holds information about the user parameters
 */
class param
{
public:
  typedef std::shared_ptr<param> sptr;

  static sptr
  make_shared(const QString& name, const QString& label,
              const QString& type, const QString& val);

  const QString&
  name();

  const QString &
  label();

  const QString&
  type();

  const QString&
  value();
  
  void
  set_value(const QString& val);

protected:
  param(const QString& name, const QString& label,
        const QString& type, const QString& val);

private:
  QString       d_name;
  QString       d_label;
  QString       d_type;
  QString       d_val;

};

#endif // PARAM_H
