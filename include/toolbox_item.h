/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_ITEM_H
#define TOOLBOX_ITEM_H

#include <yaml-cpp/yaml.h>
#include <param.h>
#include <port.h>

#include <QString>
#include <QMap>
#include <vector>
#include <memory>

class toolbox_item
{
public:
  typedef enum {
    GENERAL,
    BLOCK,
    VARIABLE,
    COMMENT,
    SETTINGS
  } toolbox_item_t;

  typedef std::shared_ptr<toolbox_item> sptr;

  void
  append_category(const QString& name);

  void
  append_category(const std::string& name);

  const QString
  name();

  const QString
  id();

  size_t
  category_depth();

  QString
  category(size_t depth);

  const QMap<QString, param::sptr>&
  parameters();

  const QMap<QString, port::sptr>&
  input_ports();

  const QMap<QString, port::sptr>&
  output_ports();

  toolbox_item_t
  type() const;

  friend std::ostream&
  operator<<(std::ostream& out, toolbox_item &item);
protected:
  YAML::Node                      d_yml_node;
  QMap<QString, port::sptr>       d_inputs;
  QMap<QString, port::sptr>       d_outputs;

  toolbox_item(const QString& id, const QString& name, const YAML::Node& yml,
               toolbox_item_t type = GENERAL);

private:
  QString                         d_id;
  QString                         d_name;
  toolbox_item_t                  d_type;
  std::vector<QString>            d_categories;
  QMap<QString, param::sptr>      d_params;

  void
  parse_params();

};

#endif // TOOLBOX_ITEM_H
