/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_H
#define TOOLBOX_H

#include "toolbox_item.h"
#include "toolbox_category.h"

#include <yaml-cpp/yaml.h>

#include <QList>
#include <QString>
#include <deque>
#include <map>
#include <cstdint>
#include <memory>

/**
 * @brief The toolbox class holds all the toolbox items that can be inserted
 * in the flowgraph
 *
 * The toolbox class holds all the toolbox items in an convinient
 * way so information about each one of them and their categories
 * (or subcategories) can be easily retrieved
 */
class toolbox
{
public:

  typedef std::shared_ptr<toolbox> sptr;

  static sptr
  make_shared();

  static sptr
  make_shared(const QString& conf);

  friend std::ostream&
  operator<< (std::ostream& out, const toolbox& t);

  toolbox_item::sptr
  operator[](const std::string& id);

  toolbox_item::sptr
  operator[](const QString& id);

  std::deque<toolbox_category::sptr>
  top_level_categories();

private:
  QList<QString>                              d_items_dirs;
  /* provides easy access to all items */
  std::map<QString, toolbox_item::sptr>       d_items;
  std::map<QString, toolbox_category::sptr>   d_categories_tree;

  toolbox();
  toolbox(const QString& conf);

  void
  parse_items();
  void
  parse_dir(const QString& dir);
  void
  parse_item(const QString& file);

  void
  parse_item_block(const QString &file, const YAML::Node& node);
};

#endif // TOOLBOX_H
