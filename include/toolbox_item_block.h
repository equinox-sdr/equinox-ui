/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TOOLBOX_ITEM_BLOCK_H
#define TOOLBOX_ITEM_BLOCK_H

#include <toolbox.h>
#include <QString>

class toolbox_item_block : public toolbox_item
{
public:
  static toolbox_item::sptr
  make_shared(const QString& id, const QString& name,
              const YAML::Node& yml);

  void
  parse_inputs();

  void
  parse_outputs();

protected:
  toolbox_item_block(const QString& id, const QString& name,
                     const YAML::Node& yml);

};

#endif // TOOLBOX_ITEM_BLOCK_H
