/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
#define UTILS_H

#include <QtWidgets>


/**
 * @brief is_dark_themed checks if the application executes on a dark themed
 * environment ot not
 *
 * @return  true of the application theme is darkish, false otherwise s
 */
static bool
is_dark_themed()
{
  QColor c = QApplication::palette().background().color();
  double darkness = 1-(0.299*c.red() + 0.587*c.green() + 0.114*c.blue())/255.0;
   if(darkness<0.5){
       return false;
   }
   return true;
}

#endif // UTILS_H
