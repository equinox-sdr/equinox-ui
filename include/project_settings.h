/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROJECT_SETTINGS_H
#define PROJECT_SETTINGS_H

#include <QString>
#include <memory>
#include <mutex>
#include <yaml-cpp/yaml.h>

class project_settings
{
public:

  typedef  std::shared_ptr<project_settings> sptr;

  static sptr
  make_shared();

  static sptr
  make_shared(const QString& eqnxfile);

  bool
  save();

  bool
  load();

  void
  set_settings_file(const QString& eqnxfile);

  QString
  settings_file();

  QString
  name();
  void
  set_name(const QString& str);

  QString
  license();
  void
  set_license(const QString& str);

  QString
  authors();
  void
  set_authors(const QString& str);

  QString
  comments();
  void
  set_comments(const QString& str);

  QString
  dir();

  void
  set_dir(const QString& str);

  void
  set_flowgraph(const YAML::Node& flowgraph);

  const YAML::Node&
  flowgraph();

  bool
  valid();

private:
  QString         d_eqnxfile;
  QString         d_name;
  QString         d_license;
  QString         d_authors;
  QString         d_comments;
  QString         d_dir;
  bool            d_valid;
  YAML::Node      d_flowgraph;
  std::mutex      d_mtx;

  project_settings(const QString& eqnxfile);

  project_settings();
};

#endif // PROJECT_SETTINGS_H
