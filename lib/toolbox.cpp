/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toolbox.h"
#include "toolbox_item_block.h"
#include "settings.h"

#include <QDebug>
#include <sys/stat.h>
#include <dirent.h>
#include <iostream>

/**
 * Creates a new empty toolbox shared pointer,
 * pasring items from directories specified at
 * the default application configuration file
 */
toolbox::sptr
toolbox::make_shared()
{
  return std::shared_ptr<toolbox>(new toolbox());
}

/**
 * Creates a new empty toolbox shared pointer,
 * pasring items from directories specified at
 * the application configuration file \p conf
 * @param conf the path of the application configuration file
 */
toolbox::sptr
toolbox::make_shared(const QString& conf)
{
  return std::shared_ptr<toolbox>(new toolbox(conf));
}

toolbox_item::sptr
toolbox::operator[](const std::string &id)
{
  try {
    return d_items.at(QString::fromStdString(id));
  }
  catch (std::out_of_range& e) {
    return nullptr;
  }
}

toolbox_item::sptr
toolbox::operator[](const QString &id)
{
  try {
    return d_items.at(id);
  }
  catch (std::out_of_range& e) {
    return nullptr;
  }
}

std::deque<toolbox_category::sptr>
toolbox::top_level_categories()
{
  std::deque<toolbox_category::sptr> d;
  for(auto i : d_categories_tree) {
    d.push_back(i.second);
  }
  return d;
}


/**
 * Creates a new toolbox, pasring items from directories specified at
 * the default application configuration file
 */
toolbox::toolbox()
{
  settings s;
  d_items_dirs = s.get_kernel_desc_paths();
  parse_items();
}

/**
 * Creates a new toolbox, pasring items from directories specified at
 * the application configuration file \p conf
 * @param conf the path of the application configuration file
 */
toolbox::toolbox(const QString& conf)
{
  settings s(conf);
  d_items_dirs = s.get_kernel_desc_paths();
  parse_items();
}

/**
 * Parses all the directories for toolbox items and creates a database that
 * describe all the toolbox categories and their corresponding toolbox items
 */
void
toolbox::parse_items()
{
  /* Get the list of supported files, searching also in sub-directories */
  for(QString i : d_items_dirs) {
    parse_dir(i);
  }

  /* Make the categories and all the sub-categories */
  for(auto i : d_items) {
    toolbox_item::sptr item = i.second;

    QString cat = i.second->category(0);
    toolbox_category::sptr cat_sptr;
    if(d_categories_tree.find(cat) != d_categories_tree.end()){
      cat_sptr = d_categories_tree[cat];
    }
    else{
      cat_sptr = toolbox_category::make_shared(cat);
      d_categories_tree[cat] = cat_sptr;
    }
    /* Traverse down to the button level subcategory */
    for(size_t j = 1; j < item->category_depth(); j++) {
      cat_sptr = cat_sptr->add_subcategory(item->category(j));
    }
    cat_sptr->add_item(item);
  }
}

std::ostream&
operator<<(std::ostream &out, const toolbox &t)
{
  out << "Total items    :   " << std::to_string(t.d_items.size()) << std::endl;

  out << "Categories:    : " << std::to_string(t.d_categories_tree.size())
      << std::endl;
  for(auto i:  t.d_categories_tree){
    out << (*i.second) << std::endl;
  }
  return out;
}


/**
 * Parses a directory for YAML files that may contain toolbox items.
 * The method recursivly searches for subdirectories
 * @param dir the directory path
 */
void
toolbox::parse_dir(const QString& dir)
{
  DIR *dirp;
  if( (dirp = opendir(dir.toStdString().c_str()) ) == nullptr) {
    return;
  }

  while(struct dirent *rd = readdir(dirp)){
    if(rd->d_name[0] =='.') {
      continue;
    }
    else if(rd->d_type == DT_DIR) {
      parse_dir(QString(dir + "/" + QString(rd->d_name)));
    }
    else{
      parse_item(QString(dir + "/" + QString(rd->d_name)));
    }
  }
  closedir(dirp);
}

/**
 * Tries to parse a toolbox item. If its is done succesfully, the method
 * appends the new item at an internal database
 * @param file the file to parse
 */
void
toolbox::parse_item(const QString& file)
{
  /* Only YAML files are supported */
  if(!file.endsWith(".yaml", Qt::CaseInsensitive)
        && !file.endsWith(".yml", Qt::CaseInsensitive)) {
    return;
  }

  try {
    YAML::Node node = YAML::LoadFile(file.toStdString());
    if(node["block"]) {
      parse_item_block(file, node);
    }
    else{
      /* TODO */
    }

  }
  catch (YAML::ParserException& e) {
    qWarning() << "Could not parse file " << file << ": " << e.what();
  }
}

/**
 * Parses an item of block type and if the parsing is successful it
 * inserts the item on the toolbox, based on the ID.
 * If there is already an item with the same ID, it does nothing.
 *
 * @param file the YAML file name describing the block. Used only for
 * reporting its name in case of an exception
 * @param node the YAML parsed node
 */
void
toolbox::parse_item_block(const QString &file, const YAML::Node& node)
{
  try {
    QString name = QString::fromStdString(node["block"]["name"].as<std::string>());
    QString id = QString::fromStdString(node["block"]["id"].as<std::string>());

    /* IDs should be unique */
    if(d_items.find(id) != d_items.end()) {
       qWarning() << "File " << file << ": ID " << id
                  << " already exists. Skipping";
       return;
    }

    toolbox_item_block::sptr item = toolbox_item_block::make_shared(id, name,
                                                                    node);
    for(YAML::const_iterator it=node["category"].begin();
        it!=node["category"].end();++it) {
      item->append_category(it->as<std::string>());
    }
    d_items[id] = item;
  }
  catch (std::exception& e) {
    qWarning() << "Could not parse file " << file << ": " << e.what();
  }
}
