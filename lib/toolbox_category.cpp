/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toolbox_category.h"

/**
 * Creates a shared pointer for a toolbox category
 * @param name the name of the category
 * @return a shared pointer with an empty category
 */
toolbox_category::sptr
toolbox_category::make_shared(const QString &name)
{
  return std::shared_ptr<toolbox_category>(new toolbox_category(name));
}

/**
 * Categories may hold items in subcategories. This method adds
 * a new subcategory and returs its shared pointer. In a subcategory
 * with the same name exists in the same level, the shared pointer of the
 * existing category is returned.
 *
 * @param name the name of the subcategory
 * @return shared pointer of the subcategory
 */
toolbox_category::sptr
toolbox_category::add_subcategory(const QString &name)
{
  if(d_subcategories.find(name) != d_subcategories.end()){
    return d_subcategories[name];
  }
  toolbox_category::sptr cat = toolbox_category::make_shared(name);
  d_subcategories[name] = cat;
  return cat;
}

/**
 * Adds a toolbox item at the gategory
 * @param item the shared pointer of the toolbox item
 */
void
toolbox_category::add_item(toolbox_item::sptr item)
{
  if(item) {
    d_items.push_back(item);
  }
}

/**
 * @return the name of the category
 */
QString
toolbox_category::name()
{
  return d_name;
}

/**
 * @return the total number of items in all levels and subcategories
 */
size_t
toolbox_category::total_items_num()
{
  size_t cnt = items_num();
  for(auto i : d_subcategories){
    cnt += i.second->total_items_num();
  }
  return cnt;
}

/**
 * @return the number of items only at the current level
 */
size_t
toolbox_category::items_num()
{
  return d_items.size();
}

/**
 * @return a deque containing the shared pointers of the first level
 * subcategories
 */
std::deque<toolbox_category::sptr>
toolbox_category::subcategories()
{
  std::deque<toolbox_category::sptr> d;
  for(auto i : d_subcategories){
    d.push_back(i.second);
  }
  return d;
}

/**
 * @return a deque with the shared pointers of the toolbox items contained
 * in the category
 */
std::deque<toolbox_item::sptr>
toolbox_category::items()
{
  return d_items;
}

/**
 * @param depth the depth specifies until which level the method
 * should count for different subcategories. if 0 is given, the method
 * counts for all the available subcategories
 *
 * @return the number of subcategories until the specified depth
 */
size_t
toolbox_category::subcategories_num(size_t depth)
{
  if(depth == 1) {
    return d_subcategories.size();
  }

  /* Follow the white rabbit... */
  if(depth == 0) {
    size_t cnt = d_subcategories.size();
    for(auto i : d_subcategories){
      cnt += i.second->subcategories_num(0);
    }
    return cnt;
  }
  else{
    size_t cnt = d_subcategories.size();
    for(auto i : d_subcategories){
      cnt += i.second->subcategories_num(depth - 1);
    }
    return cnt;
  }
}

std::ostream &
operator<<(std::ostream &out, const toolbox_category &c)
{
  out << c.d_name.toStdString() << ":" << std::endl;
  for(auto i : c.d_items){
    out << (*i) << std::endl;
  }
  for(auto i : c.d_subcategories) {
    out << (*i.second) << std::endl;
  }
  return out;
}


toolbox_category::toolbox_category(const QString &name)
  : d_name(name)
{
}
