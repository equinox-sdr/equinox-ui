/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toolbox_item_block.h"

toolbox_item::sptr
toolbox_item_block::make_shared(const QString &id,
                                const QString &name,
                                const YAML::Node& yml)
{
  return std::shared_ptr<toolbox_item>(new toolbox_item_block(id, name, yml));
}

void
toolbox_item_block::parse_inputs()
{
  /* Get the parameters from the yaml */
  for(YAML::const_iterator it = d_yml_node["block"]["inputs"].begin();
       it != d_yml_node["block"]["inputs"].end(); ++it) {
    QString port_name = QString::fromStdString(it->first.as<std::string>());


    /* FIXME: Check error*/
    if(d_inputs.contains(port_name)) {
      //qWarning() << "Parameter " << var_name << "is not unique.";
      throw std::runtime_error("Invalid yaml file");
    }

    const YAML::Node& node = it->second;
    QString port_type;
    size_t size;
    size_t msg_num;
    if(node["type"]) {
       port_type = QString::fromStdString(node["type"].as<std::string>());
    }
    else{
      /* FIXME */
      throw std::runtime_error("Invalid yaml file");
    }

    if(node["size"]) {
       size = node["size"].as<size_t>();
    }
    else{
      /* FIXME */
      throw std::runtime_error("Invalid yaml file");
    }

    if(node["msg_num"]) {
       msg_num = node["msg_num"].as<size_t>();
    }
    else{
      /* FIXME */
      throw std::runtime_error("Invalid yaml file");
    }

    d_inputs[port_name] = port::make_shared(port_name, port_type,
                                            size, msg_num, port::PORT_IN);
  }
}

void
toolbox_item_block::parse_outputs()
{
  /* Get the parameters from the yaml */
  for(YAML::const_iterator it = d_yml_node["block"]["outputs"].begin();
       it != d_yml_node["block"]["outputs"].end(); ++it) {
    QString port_name = QString::fromStdString(it->first.as<std::string>());

    /* FIXME: Check error*/
    if(d_outputs.contains(port_name)) {
      //qWarning() << "Parameter " << var_name << "is not unique.";
      throw std::runtime_error("Invalid yaml file");
    }

    const YAML::Node& node = it->second;
    QString port_type;
    size_t size;
    size_t msg_num;
    if(node["type"]) {
       port_type = QString::fromStdString(node["type"].as<std::string>());
    }
    else{
      /* FIXME */
      throw std::runtime_error("Invalid yaml file");
    }

    if(node["size"]) {
       size = node["size"].as<size_t>();
    }
    else{
      /* FIXME */
      throw std::runtime_error("Invalid yaml file");
    }

    if(node["msg_num"]) {
       msg_num = node["msg_num"].as<size_t>();
    }
    else{
      /* FIXME */
      throw std::runtime_error("Invalid yaml file");
    }

    d_outputs[port_name] = port::make_shared(port_name, port_type,
                                             size, msg_num, port::PORT_OUT);
  }
}

toolbox_item_block::toolbox_item_block(const QString &id, const QString &name,
                                       const YAML::Node& yml)
  : toolbox_item (id, name, yml, toolbox_item::BLOCK)
{
  parse_inputs();
  parse_outputs();
}
