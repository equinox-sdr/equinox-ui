/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2019  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "project_settings.h"
#include <fstream>

project_settings::sptr
project_settings::make_shared()
{
  return project_settings::sptr(new project_settings());
}

project_settings::sptr
project_settings::make_shared(const QString &eqnxfile)
{
  return project_settings::sptr(new project_settings(eqnxfile));
}

project_settings::project_settings(const QString &eqnxfile)
  : d_eqnxfile(eqnxfile)
{
}

project_settings::project_settings()
  : d_valid(false)
{
}

bool
project_settings::save()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  if(d_dir.length() == 0 || d_name.length() == 0) {
    d_valid = false;
    return false;
  }
  try {
    YAML::Node node;
    std::string f = d_dir.toStdString();
    if(f.at(f.length() -1) != '/') {
      f.append("/");
    }
    f.append(d_name.toStdString() + ".eqnx");

    std::ofstream fout(f, std::ofstream::out | std::ofstream::trunc);
    if(!fout.good()) {
      d_valid = false;
      return false;
    }

    node["project"] = d_name.toStdString();
    node["license"] = d_license.toStdString();
    node["authors"] = d_authors.toStdString();
    node["comments"] = d_comments.toStdString();
    node["flowgraph"] = d_flowgraph;
    fout << node;
    fout.close();
  }
  catch (std::exception& e) {
    d_valid = false;
    return false;
  }
  d_valid = true;
  return true;
}

bool
project_settings::load()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  if(d_eqnxfile.length() == 0) {
    d_valid = false;
    return false;
  }

  try {
    YAML::Node node = YAML::LoadFile(d_eqnxfile.toStdString());
    d_name = QString::fromStdString(node["project"].as<std::string>());
    d_authors = QString::fromStdString(node["authors"].as<std::string>());
    d_license = QString::fromStdString(node["license"].as<std::string>());
    d_comments = QString::fromStdString(node["comments"].as<std::string>());
    d_flowgraph = node["flowgraph"];
    QString tmp = d_eqnxfile;
    d_dir = tmp.remove("/" + d_name + ".eqnx");
  } catch (std::exception& e) {
    /* FIXME: Log */
    d_valid = false;
    return false;
  }
  d_valid = true;
  return true;
}

/**
 * Sets the project file
 * @param eqnxfile the project file
 */
void
project_settings::set_settings_file(const QString &eqnxfile)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_eqnxfile = eqnxfile;
}

/**
 * Returns the absolute path of the project file.
 * @note: This method should be used with care, while the project itself
 * may be moved by the user.
 *
 * @return  the absolute path of the project file
 */
QString
project_settings::settings_file()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_eqnxfile;
}

QString
project_settings::name()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_name;
}

void
project_settings::set_name(const QString& str)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_name = str;
}

QString
project_settings::license()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_license;
}

void
project_settings::set_license(const QString& str)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_license = str;
}

QString
project_settings::authors()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_authors;
}

void
project_settings::set_authors(const QString& str)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_authors = str;
}

QString
project_settings::comments()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_comments;
}

void
project_settings::set_comments(const QString& str)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_comments = str;
}

QString
project_settings::dir()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_dir;
}

void
project_settings::set_dir(const QString &str)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_dir = str;
}

void
project_settings::set_flowgraph(const YAML::Node &flowgraph)
{
  std::lock_guard<std::mutex> lock(d_mtx);
  d_flowgraph = flowgraph;
}

const YAML::Node &
project_settings::flowgraph()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return  d_flowgraph;
}

bool
project_settings::valid()
{
  std::lock_guard<std::mutex> lock(d_mtx);
  return d_valid;
}
