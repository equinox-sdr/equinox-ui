/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "port.h"


port::sptr
port::make_shared(const QString &name, const QString &data_type,
                             size_t size, size_t msg_num,
                             port::port_type_t type)
{
  return std::shared_ptr<port>(new port(name, data_type, size, msg_num, type));
}

port::port(const QString &name, const QString &data_type, size_t size,
           size_t msg_num, port::port_type_t type)
  : d_name(name),
    d_data_type(data_type),
    d_size(size),
    d_msg_num(msg_num),
    d_type(type)
{
}
