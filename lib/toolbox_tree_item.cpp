/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toolbox_tree_item.h"


toolbox_tree_item::toolbox_tree_item(const QVariant &data,
                                     const QString &id,
                                     toolbox_tree_item *parent_item)
  : d_item_data(data),
    d_id(id),
    d_parent_item(parent_item)
{
}

toolbox_tree_item::~toolbox_tree_item()
{
  qDeleteAll(d_child_items);
}

void
toolbox_tree_item::append_child(toolbox_tree_item *child)
{
  if(child) {
    d_child_items.append(child);
  }
}

toolbox_tree_item *
toolbox_tree_item::child(int row)
{
  return d_child_items.value(row);
}

int
toolbox_tree_item::children() const
{
  return d_child_items.count();
}

int
toolbox_tree_item::columns() const
{
  return 1;
}

QVariant
toolbox_tree_item::data(int column) const
{
  if(column > 0) {
    return QVariant();
  }
  return d_item_data;
}

int
toolbox_tree_item::row() const
{
  if(d_parent_item) {
    return d_parent_item->d_child_items.indexOf(
          const_cast<toolbox_tree_item *>(this));
  }
  return 0;
}

toolbox_tree_item *
toolbox_tree_item::parent()
{
  return d_parent_item;
}

QString
toolbox_tree_item::id() const
{
  return d_id;
}
