/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include <QList>
#include <iostream>

/**
 * @brief settings::settings Get the application configuration
 * from the default user path
 */
settings::settings()
  : d_settings(QSettings::UserScope, "equinox-sdr", "equinox")
{
  check();
}

/**
 * @brief settings::settings Get the application configuration from a
 * specific configuration file
 * @param conf the configuration file path
 */
settings::settings(const QString &conf)
  : d_settings(conf, QSettings::NativeFormat)
{
  check();
}

/**
 * @brief settings::get_kernel_desc_paths retrieve the paths where
 * graphical descriptions of kernels are stored
 * @return  a list with directories where
 * graphical descriptions of kernels are stored
 */
QList<QString>
settings::get_kernel_desc_paths()
{
  d_settings.beginGroup("kernels");
  QList<QString> paths;
  QStringList custom_paths
      = d_settings.value("paths",
                         QVariant(QStringList
                                  ({"/usr/share/equinox/kernels"}))
                        ).value<QStringList>();
  for(const QString &i : custom_paths) {
    paths.append(i);
  }
  paths.removeDuplicates();
  d_settings.endGroup();
  return paths;
}

/**
 * @return the flowgraph canvas size
 */
QSize
settings::get_canvas_size()
{
  d_settings.beginGroup("mainview");
  QSize s = d_settings.value("canvas_size", QSize(1024, 1024)).toSize();
  d_settings.endGroup();
  return s;
}

/**
 *
 * @return the window geometry
 */
QByteArray
settings::win_geometry()
{
  d_settings.beginGroup("mainview");
  QByteArray b = d_settings.value("geometry").toByteArray();
  d_settings.endGroup();
  return b;
}

/**
 *
 * @return the window state
 */
QByteArray
settings::win_state()
{
  d_settings.beginGroup("mainview");
  QByteArray b = d_settings.value("winstate").toByteArray();
  d_settings.endGroup();
  return b;
}

/**
 * @return  the state of the main splitter
 */
QByteArray
settings::splitter_state()
{
  d_settings.beginGroup("mainview");
  QByteArray b = d_settings.value("splitterstate").toByteArray();
  d_settings.endGroup();
  return b;
}

/**
 * @brief settings::set_win_geometry stores the window geometry
 * @param b the windowgeometry inforamtion
 */
void
settings::set_win_geometry(QByteArray b)
{
  d_settings.beginGroup("mainview");
  d_settings.setValue("geometry", b);
  d_settings.endGroup();
}

/**
 * @brief settings::set_win_state Stores the window state
 * @param b window state
 */
void
settings::set_win_state(QByteArray b)
{
  d_settings.beginGroup("mainview");
  d_settings.setValue("winstate", b);
  d_settings.endGroup();
}

void
settings::set_splitter_state(QByteArray b)
{
  d_settings.beginGroup("mainview");
  d_settings.setValue("splitterstate", b);
  d_settings.endGroup();
}

/**
 * @brief Stores the opened projects
 * @param projects a list with the opened project files
 */
void
settings::set_open_projects(const QStringList& projects)
{
  d_settings.beginGroup("projects");
  d_settings.setValue("open", projects);
  d_settings.endGroup();
}

/**
 * @brief Gets the last opened projects
 * @return  a list containing the paths of the last opened projects.
 * If none, it returns an empty list
 */
QStringList
settings::open_projects()
{
  d_settings.beginGroup("projects");
  QStringList open = d_settings.value("open").value<QStringList>();
  d_settings.endGroup();
  return open;
}

/**
 * Checks the configuration file and sets the necessary default values
 */
void
settings::check()
{
  bool found;

  /* Configuration of the main view */
  d_settings.beginGroup("mainview");
  found = d_settings.contains("canvas_size");
  if(!found) {
    d_settings.setValue("canvas_size", QSize(1024, 1024));
  }
  d_settings.endGroup();

  /* Kernels specific configuration */
  d_settings.beginGroup("kernels");
  found = d_settings.contains("paths");
  if(!found) {
    d_settings.setValue("paths",
                        QVariant(QStringList
                                  ({"/usr/share/equinox/kernels"})));
  }
  d_settings.endGroup();
}
