/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "toolbox_item.h"


/**
 * A toolbox item
 * @param id the unique ID
 * @param name the name of the toolbox item
 * @param yml the YAML node describing the toolbox item
 * @param type the type of the item
 */
toolbox_item::toolbox_item(const QString &id, const QString &name,
                           const YAML::Node& yml,
                           toolbox_item_t type)
  : d_id(id),
    d_name(name),
    d_type(type),
    d_yml_node(YAML::Clone(yml))
{
  parse_params();
}

void
toolbox_item::parse_params()
{
  /* Get the parameters from the yaml */
  for(YAML::const_iterator it = d_yml_node["parameters"].begin();
      it != d_yml_node["parameters"].end(); ++it) {

    QString var_name = QString::fromStdString(it->first.as<std::string>());

    /* FIXME: Check error*/
    if(d_params.contains(var_name)) {
      //qWarning() << "Parameter " << var_name << "is not unique.";
      throw std::runtime_error("Invalid yaml file");
    }

    QString var_type;
    QString var_label;
    QString var_val = "";

    const YAML::Node& node = it->second;
    if(node["label"]) {
      var_label = QString::fromStdString(node["label"].as<std::string>());
    }
    else{
      var_label = var_name;
    }

    if(node["value"]) {
      var_val = QString::fromStdString(node["value"].as<std::string>());
    }
    var_type = QString::fromStdString(node["type"].as<std::string>());
    d_params[var_name] = param::make_shared(var_name, var_label,
                                            var_type, var_val);
  }
}

/**
 * Append a category that this toolbox item belongs.
 * Each category added is considered a sub-category of the previous
 * @param name the name of the category
 */
void
toolbox_item::append_category(const QString &name)
{
  d_categories.push_back(name);
}

/**
 * Append a category that this toolbox item belongs.
 * Each category added is considered a sub-category of the previous
 * @param name the name of the category
 */
void
toolbox_item::append_category(const std::string &name)
{
  append_category(QString::fromStdString(name));
}

const QString
toolbox_item::name()
{
  return d_name;
}

const QString
toolbox_item::id()
{
  return d_id;
}

size_t
toolbox_item::category_depth()
{
  return d_categories.size();
}

QString
toolbox_item::category(size_t depth)
{
  return d_categories[depth];
}

const QMap<QString, param::sptr> &
toolbox_item::parameters()
{
  return d_params;
}

const QMap<QString, port::sptr> &
toolbox_item::input_ports()
{
  return d_inputs;
}

const QMap<QString, port::sptr> &
toolbox_item::output_ports()
{
  return d_outputs;
}

toolbox_item::toolbox_item_t
toolbox_item::type() const
{
  return d_type;
}

std::ostream &
operator<<(std::ostream &out, toolbox_item &item)
{
  out << "Name     : " << item.d_name.toStdString() << std::endl
      << "ID       : " << item.d_id.toStdString();
  return out;
}
