/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "param.h"

/**
 * @brief param::make_shared creates a new shared pointer for a parameter
 * @param name the name of the parameter
 * @param type the type of the parameter
 * @param val the value
 * @return shared pointer of a parameter
 */
param::sptr
param::make_shared(const QString &name, const QString &label,
                   const QString &type, const QString &val)
{
  return param::sptr(new param(name, label, type, val));
}

/**
 *
 * @return  the name of the parameter
 */
const QString &
param::name()
{
  return d_name;
}

/**
 *
 * @return  the label of the parameter
 */
const QString &
param::label()
{
  return d_label;
}

/**
 * @return  a C++ valid type of the parameter
 */
const QString &
param::type()
{
  return d_type;
}

/**
 * @return  the value of the parameter
 */
const QString &
param::value()
{
  return d_val;
}

/**
 * @param val sets a new value at the parameter
 */
void
param::set_value(const QString &val)
{
  d_val = val;
}

param::param(const QString &name, const QString &label,
             const QString &type, const QString &val)
  : d_name(name),
    d_label(label),
    d_type(type),
    d_val(val)
{
}
