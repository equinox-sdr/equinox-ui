#include <inja.hpp>
#include <yaml-cpp/yaml.h>


int
main(int argc, char *argv[])
{
  inja::Environment env = inja::Environment();

  inja::json data;
  inja::Template templ = env.parse_template("./test.yaml");
  std::string  r = env.render_template(templ, data);
  std::cout << r << std::endl;
  return 0;
}
